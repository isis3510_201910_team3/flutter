import 'Vacuna.dart';

class Hijo {
  String id,nombre, fecha;
  int peso,altura,edad, sexo,compartido;
  List vacunas;

  Hijo(this.id,this.nombre, this.fecha, this.edad,this.peso, this.altura, this.sexo, this.compartido, this.vacunas);

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["nombre"] = nombre;
    map["edad"] = edad;
    map["compartido"] = compartido;
    map["fechaNacimiento"] = fecha;
    map["peso"] = peso;
    map["altura"] = altura;
    map["sexo"] = sexo;
    return map;
  }

  Map<String, dynamic> toMapFirebase() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["nombre"] = nombre;
    map["edad"] = edad;
    map["compartido"] = compartido;
    map["fechaNacimiento"] = fecha;
    map["peso"] = peso;
    map["altura"] = altura;
    map["sexo"] = sexo;
    map["vacunas"] = vacunas;
    return map;
  }
}
