
class Tareas {
  String id;
  String categoria;
  String prioridad;
  String titulo;
  String descripcion;
  String fecha;
  String hijo;

  Tareas(this.id,this.categoria, this.prioridad, this.titulo, this.descripcion, this.fecha, this.hijo);

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["hijo"] = hijo;
    map["categoria"] = categoria;
    map["prioridad"] = prioridad;
    map["titulo"] = titulo;
    map["descripcion"] = descripcion;
    map["fecha"] = fecha;
    return map;
  }

}
