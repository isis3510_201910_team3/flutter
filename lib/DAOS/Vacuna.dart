class Vacuna {
  String nombre;
  int aplicada,edad, dosis;

  Vacuna(this.aplicada, this.nombre, this.edad,this.dosis);

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["aplicada"] = aplicada;
    map["nombre"] = nombre;
    map["edad"] = edad;
    map["dosis"] = dosis;
    return map;
  }
}