class Tutor {
  String nombre, email,cumple, eps;
  int sexo,numHijos,edad,estrato;


  Tutor(this.nombre, this.email, this.cumple,this.edad, this.eps, this.sexo, this.numHijos,this.estrato);

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["nombre"] = nombre;
    map["correo"] = email;
    map["fechaNacimiento"] = cumple;
    map["edad"] = edad;
    map["numeroHijos"] = numHijos;
    map["estrato"] = estrato;
    map["sexo"] = sexo;
    if(eps!=null)
      map["eps"] = eps;
    else
      map["eps"] = "";
    return map;
  }
}