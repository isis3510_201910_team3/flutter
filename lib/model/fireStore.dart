import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:bhelp/DAOS/Hijo.dart';
import 'package:bhelp/DAOS/Tutor.dart';
import 'package:bhelp/DAOS/Tareas.dart';
import 'package:bhelp/DAOS/Vacuna.dart';
import 'package:bhelp/main.dart';

Firestore datos = Firestore.instance;

Widget getEspecialistas() {
  return StreamBuilder<QuerySnapshot>(
    stream: datos.collection('especialistas').snapshots(),
    builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
      if (snapshot.hasError) return new Text('Error: ${snapshot.error}');
      switch (snapshot.connectionState) {
        case ConnectionState.waiting:
          return new Text('Cargando...');
        default:
          return new ListView(
            shrinkWrap: true,
            itemExtent: 80.0,
            children: snapshot.data.documents.map((DocumentSnapshot document) {
              return new ListTile(
                contentPadding: EdgeInsets.all(20.0),
                title: new Text(document['nombre']),
                subtitle: new Text(document['telefono']),
                leading: Image.asset('assets/image/doctor.png'),
              );
            }).toList(),
          );
      }
    },
  );
}


crearHijo(Hijo hijo) {
  datos
      .collection('/tutores/'+sharedPreferencesFull.getString('correo')+'/hijos')
      .document(hijo.nombre + "-" + hijo.fecha)
      .setData(hijo.toMapFirebase());
}

updateHijoFires(Hijo hijoActual,Hijo hijoNuevo) async {
  String correo=sharedPreferencesFull.getString('correo');

  datos
      .collection('/tutores/'+correo+'/hijos')
      .document(hijoActual.id).updateData(hijoNuevo.toMapFirebase());

}

borrarHijo(Hijo hijo) async {
  datos
      .collection('/tutores/'+sharedPreferencesFull.getString('correo')+'/hijos')
      .document(hijo.id)
      .delete();
}

Future<List<Hijo>> getHijos() async {
  List<Hijo> hijos = new List();
  QuerySnapshot coleccion = await datos
      .collection('/tutores/'+sharedPreferencesFull.getString('correo')+'/hijos')
      .getDocuments();
  var list = coleccion.documents;
  Hijo actual =new Hijo("","", "", 0, 0 , 0, 0, 0, List());
  for (var doc in list) {
    //ERROR QUE TOCA EVITAR
    actual = Hijo(doc['id'],doc['nombre'], doc['fechaNacimiento'], doc['edad'], doc['peso'], doc['altura'], doc['sexo'], doc['compartido'], doc['']);
    hijos.add(actual);
  }
  return hijos;
}

Future<String> getTutor(String correo) async {
  print("El coreo "+ correo);
  String tutor=null;
  DocumentSnapshot doc = await datos.collection('tutores').document(correo).get();
  if(doc!=null){
    try{
      tutor=doc['correo'];
    }
    catch(e){

    }
  }
  return tutor;
}
crearTareaCategoria(Tareas tarea) {
  datos
      .collection(
      '/tutores/' + sharedPreferencesFull.getString('correo') + '/tareas' +
          tarea.categoria)
      .document(tarea.id)
      .setData(tarea.toMap());
}
crearTareaHijo(Tareas tarea) {
  datos
      .collection('/tutores/' + sharedPreferencesFull.getString('correo') + '/hijos/' + tarea.hijo + '/tareas')
      .document(tarea.id)
      .setData(tarea.toMap());
  if(tarea.prioridad=="Alta"){
    crearTareaPrioridadAlta(tarea);
  }
}

crearTareaPrioridadAlta(Tareas tarea) {
  datos
      .collection('/tutores/'+sharedPreferencesFull.getString('correo')+'/hijos/' + tarea.hijo+'/tareasPrioridadAlta')
      .document(tarea.id)
      .setData(tarea.toMap());
}

updateTarea(Tareas tareaActual,Tareas tareaNueva) async {
  String correo=sharedPreferencesFull.getString('correo');
  if(tareaActual.categoria != tareaNueva.categoria){
    await borrarTareaCategoria(tareaActual);
    await crearTareaCategoria(tareaNueva);
  }
  else {
    datos
        .collection('/tutores/' + correo + '/tareas' +tareaNueva.categoria)
        .document(tareaActual.id).updateData(tareaNueva.toMap());
  }
  if(tareaActual.hijo != tareaNueva.hijo){
    await borrarTareaHijo(tareaActual);
    await crearTareaHijo(tareaNueva);
  }
  else {
    datos
        .collection('/tutores/'+correo+'/hijos/' + tareaNueva.hijo+'/tareas')
        .document(tareaActual.id).updateData(tareaNueva.toMap());
    if(tareaActual.prioridad!=tareaNueva.prioridad &&tareaNueva.prioridad=="Alta"){
      print("Entra en 1");
      crearTareaPrioridadAlta(tareaNueva);
    }
    else if(tareaActual.prioridad!=tareaNueva.prioridad &&tareaActual.prioridad=="Alta"){
      print("Entra en 2");
      borrarTareaPrioridad(tareaActual);
    }
    else if(tareaNueva.prioridad=="Alta"){
      print("Entra en 3");
      datos
          .collection('/tutores/'+correo+'/hijos/' + tareaNueva.hijo+'/tareasPrioridadAlta')
          .document(tareaActual.id).updateData(tareaNueva.toMap());
    }
  }
}


borrarTareaCategoria(Tareas tarea) async {
  datos
      .collection('/tutores/'+sharedPreferencesFull.getString('correo')+'/tareas' + tarea.categoria)
      .document(tarea.id)
      .delete();
}
borrarTareaHijo(Tareas tarea) async {
  datos
      .collection('/tutores/' + sharedPreferencesFull.getString('correo') + '/hijos/' + tarea.hijo + '/tareas')
      .document(tarea.id)
      .delete();
  if (tarea.prioridad == "Alta") {
    borrarTareaPrioridad(tarea);
  }
}
borrarTareaPrioridad(Tareas tarea) async {
      datos
          .collection('/tutores/'+sharedPreferencesFull.getString('correo')+'/hijos/' + tarea.hijo+'/tareasPrioridadAlta')
          .document(tarea.id)
          .delete();
}


Future<List<Tareas>> getTareas(String tipo) async {
  List<Tareas> tareas = new List();
  QuerySnapshot coleccion = await datos
      .collection('/tutores/'+sharedPreferencesFull.getString('correo')+'/tareas' + tipo)
      .getDocuments();
  var list = coleccion.documents;
  Tareas actual = new Tareas("","", "", "", "", "", "");;
  for (var doc in list) {
   // actual.id=
     actual = new Tareas(doc['id'],tipo, doc['prioridad'], doc['titulo'],
        doc['descripcion'], doc['fecha'], doc['hijo']);
    actual.id=doc['id'];
    tareas.add(actual);
  }
  return tareas;
}

Future<List<Tareas>> getTareasPrioridad(String prioridad,String idHijo) async {
  List<Tareas> tareas = new List();
  QuerySnapshot coleccion = await datos
      .collection('/tutores/'+sharedPreferencesFull.getString('correo')+'/hijos/'+idHijo+'/tareasPrioridadAlta')
      .getDocuments();
  var list = coleccion.documents;
  Tareas data= new Tareas("","", "", "", "", "", "");
  for (var doc in list) {
    //ERROR QUE TOCA EVITAR
    data.titulo=doc['titulo'];
    data.descripcion=doc['descripcion'];
    data.fecha= doc['fecha'];
    data.id=doc['id'];
    tareas.add(data);
  }
  return tareas;
}

crearTutor(Tutor tuto){
  datos
      .collection('/tutores')
      .document(tuto.email).setData(tuto.toMap());
}

Widget getVacunas(){
  return StreamBuilder<QuerySnapshot>(
    stream: datos.collection('vacunas').snapshots(),
    builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
      if (snapshot.hasError) return new Text('Error: ${snapshot.error}');
      switch (snapshot.connectionState) {
        case ConnectionState.waiting:
          return new Text('Cargando...');
        default:
          return new ListView(
            shrinkWrap: true,
            itemExtent: 80.0,
            children: snapshot.data.documents.map((DocumentSnapshot document) {
              return new ListTile(
                title: new Text(document['nombre']),
                subtitle: new Text('Dosis: '+ document['dosis']),
                leading: Checkbox(value: true, onChanged: null),
              );
            }).toList(),
          );
      }
    },
  );
}

Future<List<Vacuna>> getTodasVacunas() async {
  List<Vacuna> vacunas = new List();
  QuerySnapshot coleccion = await datos
      .collection('vacunas')
      .getDocuments();
  var list = coleccion.documents;
  for (var doc in list) {
    //ERROR QUE TOCA EVITAR
    Vacuna actual = new Vacuna(0,doc['nombre'], int.parse(doc['edad']), int.parse(doc['dosis']));
    actual.nombre=doc['nombre'];
    vacunas.add(actual);
  }
  return vacunas;
}

Future<List<Vacuna>> getVacunasEdad(String edad) async {
  List<Vacuna> vacunas = new List();
  QuerySnapshot coleccion = await datos
      .collection('vacunas')
      .getDocuments();
  var list = coleccion.documents;
  for (var doc in list) {
    if(int.parse(doc['edad'])<=int.parse(edad)){
      Vacuna actual = new Vacuna(0,doc['nombre'], doc['edad'], doc['dosis']);
      actual.nombre=doc['nombre'];
      vacunas.add(actual);
    }

  }
  return vacunas;
}

Future<List<Vacuna>> getTodasVacunasHijo(String id) async {
  List<Vacuna> vacunas = new List();
  QuerySnapshot collectionReference = await datos
      .collection('/tutores/'+sharedPreferencesFull.getString('correo')+'/hijos').getDocuments();


  var list = collectionReference.documents;

  for (var doc in list) {

    if(doc.documentID== id) {
      List<dynamic> map = doc['vacunas'];
      map.forEach((v) {
        Vacuna actual = new Vacuna(
            v['aplicada'], v['nombre'], v['edad'], v['dosis']);
        actual.nombre = v['nombre'];
        vacunas.add(actual);
      }
      );
    }

  }

  return vacunas;
}

Future<String> getInfo(String nombre, String variable, String tipo) async {
  if(nombre!=null && nombre!= "") {
    bool encontrado = false;
    String nombreEncontrado = "";
    QuerySnapshot coleccion = await datos
        .collection(tipo)
        .getDocuments();
    var list = coleccion.documents;
    for (var doc in list) {
      if (!encontrado) {
        if (nombre.toLowerCase() == doc['tema'].toString().toLowerCase() || (doc['tema'].toString().toLowerCase()).contains(nombre.toLowerCase())
        || (doc['informacion'].toString().toLowerCase()).contains(nombre.toLowerCase())) {
          nombreEncontrado = doc['informacion'];
          variable = nombreEncontrado;
          encontrado = true;
        }
      }
    }
    if (encontrado) {
      print(nombreEncontrado);
      print('var ' + variable);
      return (nombreEncontrado);
    }
    else {
      return ('No hay información asociada al tema buscado.');
    }
  }
  else {
    return ('Ingrese un tema sobre el que desea información.');
  }
}


crearAutorizacion(String correo, String id) {
  String propietario=sharedPreferencesFull.getString('correo');

  var map = new Map<String, dynamic>();
  map["dueño"] = propietario;
  map["idHijo"] = id;

  datos
      .collection('/tutores/'+correo+'/compartido')
      .document(propietario+","+id)
      .setData(map);
}
