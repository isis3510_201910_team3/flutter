import 'dart:async';
import 'dart:io' as io;
import 'package:bhelp/DAOS/Tareas.dart';
import 'package:bhelp/DAOS/Hijo.dart';
import 'package:bhelp/DAOS/Vacuna.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper {
  static final DatabaseHelper _instance = new DatabaseHelper.internal();
  factory DatabaseHelper() => _instance;
  static Database _db;

  // Constructor de la clase
  DatabaseHelper.internal();

  // Metodo encargado de omprobar la existencia de la base de datos
  Future<Database> get _darDB async {
    if (_db != null) return _db;
    _db = await initDb();
    return _db;
  }

  // Metodo para iniciar la base de datos
  initDb() async {
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "LocalDBhelp2.db");
    var theDb = await openDatabase(path, version: 1, onCreate: _onCreate);
    return theDb;
  }

  // Se crean las primeras tablas  en la base
  void _onCreate(Database db, int version) async {
    // When creating the db, create the table

    await db.execute(
        "CREATE TABLE TablaHijos(id TEXT PRIMARY KEY, nombre TEXT, fechaNacimiento TEXT,  edad INTEGER, peso INTEGER, altura INTEGER, sexo BOOLEAN, compartido INTEGER)");
    await db.execute(
        "CREATE TABLE TablaTareas(id TEXT PRIMARY KEY, categoria TEXT, prioridad TEXT, titulo TEXT,descripcion TEXT, fecha TEXT, hijo TEXT)");
  }

  void formatear() async {
    var db = await _darDB;
    await db.execute(
        "DROP TABLE TablaHijos");
    await db.execute(
        "CREATE TABLE TablaHijos(id TEXT PRIMARY KEY, nombre TEXT, fechaNacimiento TEXT,  edad INTEGER, peso INTEGER, altura INTEGER, sexo BOOLEAN, compartido INTEGER)");
  }

  Future<int> saveTarea(Tareas tarea) async {
    var dbTarea = await _darDB;
    int res = await dbTarea.insert("TablaTareas", tarea.toMap());
    return res;
  }

  Future<int> saveHijo(Hijo hijo) async {
    var dbHijo = await _darDB;
    int res = await dbHijo.insert("TablaHijos", hijo.toMap());
    return res;
  }

   sincronizar(List<Hijo> hijos,List<Tareas> tareas1,List<Tareas> tareas2,List<Tareas> tareas3) async {
    var dbHijo = await _darDB;
    for(Hijo hijo in hijos){
      await dbHijo.insert("TablaHijos", hijo.toMap());
    }
    for(Tareas tarea in tareas1){
      await dbHijo.insert("TablaTareas", tarea.toMap());
    }
    for(Tareas tarea in tareas2){
      await dbHijo.insert("TablaTareas", tarea.toMap());
    }
    for(Tareas tarea in tareas3){
      await dbHijo.insert("TablaTareas", tarea.toMap());
    }
  }
  Future<List<Tareas>> getTareas(tipo) async {
    var dbTarea = await _darDB;
    List<Map> list = await dbTarea
        .rawQuery('SELECT * FROM TablaTareas WHERE categoria=?', [tipo]);
    List<Tareas> tareas = new List();
    for (int i = 0; i < list.length; i++) {
      var tarea = new Tareas(list[i]["titulo"]+"-"+list[i]["fecha"],list[i]["categoria"], list[i]["prioridad"],
          list[i]["titulo"], list[i]["descripcion"], list[i]["fecha"], list[i]["hijo"]);
      tarea.id = list[i]["id"];
      tareas.add(tarea);
    }
    print(tareas.length);
    return tareas;
  }

  Future<List<Tareas>> getTareasPrioridad(prioridad) async {
    var dbTarea = await _darDB;
    List<Map> list = await dbTarea
        .rawQuery('SELECT * FROM TablaTareas WHERE prioridad=?', [prioridad]);
    List<Tareas> tareas = new List();
    for (int i = 0; i < list.length; i++) {
      var tarea = new Tareas(list[i]["titulo"]+"-"+list[i]["fecha"],list[i]["categoria"], list[i]["prioridad"],
          list[i]["titulo"], list[i]["descripcion"], list[i]["fecha"], list[i]["hijo"]);
      tarea.id = list[i]["id"];
      tareas.add(tarea);
    }
    return tareas;
  }

  Future<List<Hijo>> getHijos() async {
    var dbHijo = await _darDB;
    List<Map> list = await dbHijo.rawQuery('SELECT * FROM TablaHijos');
    List<Hijo> hijos = new List();
    for (int i = 0; i < list.length; i++) {
      var hijo = new Hijo(list[i]["id"],list[i]["nombre"], list[i]["fechaNacimiento"], list[i]["edad"],list[i]["peso"], list[i]["altura"],
          list[i]["sexo"], list[i]["compartido"], new List());
      hijos.add(hijo);
    }
    return hijos;
  }

  Future<List<String>> getNombreHijos() async {
    var dbTarea = await _darDB;
    List<Map> list = await dbTarea
        .rawQuery('SELECT * FROM TablaHijos');
    List<String> hijos = new List();
    String hijo="";
    for (int i = 0; i < list.length; i++) {
      hijo = list[i]["id"];
      hijos.add(hijo);
    }
    return hijos;
  }

  Future<int> deleteTareas(Tareas tarea) async {
    var dbTarea = await _darDB;
    int res = await dbTarea
        .rawDelete('DELETE FROM TablaTareas WHERE id = ?', [tarea.id]);
    return res;
  }

  Future<int> deleteHijos(Hijo hijo) async {
    var dbHijo = await _darDB;
    int res = await dbHijo
        .rawDelete('DELETE FROM TablaHijos WHERE id = ?', [hijo.id]);
    return res;
  }

  Future<bool> update(Tareas tarea) async {
    var dbTarea = await _darDB;
    print("LAAAAAAAA tarea que llega es "+tarea.id.toString()+" y tiene titulo: "+ tarea.titulo);
    int res = await dbTarea.update("TablaTareas", tarea.toMap(),
        where: "id = ?", whereArgs: [tarea.id]);
    return res > 0 ? true : false;
  }

  Future<bool> updateHijo(Hijo hijo) async {
    var dbHijo = await _darDB;
    int res = await dbHijo.update("TablaHijos", hijo.toMap(),
        where: "id = ?", whereArgs: [hijo.id]);
    return res > 0 ? true : false;
  }
}
