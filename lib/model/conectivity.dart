// Librerias
import 'package:connectivity/connectivity.dart' ;

//Metodo para verificar la conexion en ese momento- funciona cuando se toca el botoon

Future<bool> coneccionState() async{
  bool resultado =false;
  var estado = await (Connectivity().checkConnectivity());
  if (estado == ConnectivityResult.mobile) {
    resultado=true;
  }
  else if (estado== ConnectivityResult.wifi) {
    resultado=true;
  }

  return resultado;
}