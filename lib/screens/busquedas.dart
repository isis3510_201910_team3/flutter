import 'package:flutter/material.dart';
import 'package:bhelp/widgets/Drawer.dart' as drawer;
import 'package:bhelp/presenters/visualizaPantalla.dart';
import 'package:bhelp/main.dart';
import 'package:bhelp/model/fireStore.dart' as fire;
import 'package:bhelp/widgets/AlertDialog.dart';
import 'package:bhelp/model/conectivity.dart';
import 'package:flutter/services.dart';


class BusquedaPage extends StatefulWidget {
  BusquedaPage({Key key, this.title5}) : super(key: key);
  final String title5;

  @override
  _BusquedaPageState createState() => _BusquedaPageState();
}

class _BusquedaPageState extends State<BusquedaPage> implements Vista {

  VisualizaPantalla homePresenter;

  @override
  void initState() {
    super.initState();
    homePresenter = new VisualizaPantalla(this);

    _c = new TextEditingController();
    super.initState();
  }

  displayRecord() {
    homePresenter.updateScreen();
  }

  TextEditingController _c ;
  TextEditingController _c2 ;
  TextEditingController _c3 ;


  @override
  void dispose(){
    _c?.dispose();
    super.dispose();

    _c2?.dispose();
    super.dispose();

    _c3?.dispose();
    super.dispose();
  }

  var busqueda;
  var busqueda2;
  var busqueda3;
  var _formKey = GlobalKey<FormState>();
  var _formKey2 = GlobalKey<FormState>();
  var _formKey3= GlobalKey<FormState>();
  String resultado;
  String resultado2;
  String resultado3;

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          bottom: TabBar(
            tabs: [
              Tab(icon: Icon(Icons.child_care), text: "Cuidado",),
              Tab(icon: Icon(Icons.calendar_today), text: "Etapas"),
              Tab(icon: Icon(Icons.local_pharmacy), text: "Salud"),
            ],
          ),
          title: Text('Consulta información'),
        ),
        drawer: drawer.menu(context),
        body: TabBarView(
          children: [

            //1
            Center(
              child:
              Padding(padding: EdgeInsets.all(20),
                child: SingleChildScrollView(
                  child:
                  Form(
                    key:_formKey,
                    child: Column(
                      children: <Widget>[
                        Text(' '),
                        Text(' '),
                        new Image.asset(
                          'assets/image/search.png' ,width: 70,height: 70,
                        ),
                        Text(' '),
                        Text(' '),
                        Text(
                          '  Busca información sobre el cuidado de tu hijo ',
                          style: TextStyle(
                              color: Colors.black54, fontSize: 16.0),
                        ),
                        Text(' '),
                        Text(' '),
                        TextFormField(
                          controller: _c,
                          inputFormatters: [
                            LengthLimitingTextInputFormatter(50),
                          ],
                          onSaved: (input){
                            print(busqueda
                            );
                            busqueda=input;
                          },
                          decoration: InputDecoration(
                            labelText: 'Consulta aquí',
                            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 50.0, 10.0),
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
                          ),
                        ),
                        SizedBox(height: 8.0),
                        Text(' '),
                        Text(' '),
                        Material(
                          borderRadius: BorderRadius.circular(20.0),
                          shadowColor: menta,
                          elevation: 5.0,
                          color: menta,
                          child: MaterialButton(
                            minWidth: 200,
                            height: 42.0,
                            onPressed:() {
                              setState(()
                              {
                                busqueda=_c.text;
                                _formKey.currentState.save();

                                _buscar(context, busqueda, 'cuidado', resultado).then((result) {
                                  print(result);
                                  setState(() {
                                    resultado = result;
                                  });
                                });
                                _c.text="";
                              });
                            },
                            child: Text('Consultar', style: TextStyle(color: Colors.white, fontSize: tamanioTexto)),
                          ),
                        ),
                        Text(' '),
                        Text(' '),

                        new Text((busqueda==null || busqueda== "")?"":"Buscando resultados para $busqueda", style: TextStyle(fontSize: tamanioTexto,fontWeight: FontWeight.bold, ), textAlign: TextAlign.justify,),
                        Text(' '),
                        new Text((resultado==null)?"":"$resultado", style: TextStyle(fontSize: tamanioTexto), textAlign: TextAlign.justify,),
                        Text(' '),
                      ],
                    ),
                  ),
                ),
              ),

            ),


            //2

            Center(
              child: Padding(
                padding: EdgeInsets.all(20),
                child: SingleChildScrollView(
                  child:
                  Form(
                    key:_formKey2,
                    child: Column(
                      children: <Widget>[
                        Text(' '),
                        Text(' '),
                        new Image.asset(
                          'assets/image/search.png' ,width: 70,height: 70,
                        ),
                        Text(' '),
                        Text(' '),
                        Text(
                          '  Busca información sobre el cuidado de tu hijo ',
                          style: TextStyle(
                              color: Colors.black54, fontSize: 16.0),
                        ),
                        Text(' '),
                        Text(' '),
                        TextFormField(
                          controller: _c2,
                          inputFormatters: [
                            LengthLimitingTextInputFormatter(50),
                          ],
                          onSaved: (input){
                            busqueda2=input;
                          },
                          decoration: InputDecoration(
                            labelText: 'Consulta aquí',
                            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 50.0, 10.0),
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
                          ),
                        ),
                        SizedBox(height: 8.0),
                        Text(' '),
                        Text(' '),
                        Material(
                          borderRadius: BorderRadius.circular(20.0),
                          shadowColor: menta,
                          elevation: 5.0,
                          color: menta,
                          child: MaterialButton(
                            minWidth: 200,
                            height: 42.0,
                            onPressed:() {
                              setState(()
                              {
                                busqueda2=_c2.text;
                                _formKey2.currentState.save();

                                _buscar(context, busqueda, 'cuidado', resultado).then((result) {
                                  print(result);
                                  setState(() {
                                    resultado2 = result;
                                  });
                                });
                                _c2.text="";
                              });
                            },
                            child: Text('Consultar', style: TextStyle(color: Colors.white, fontSize: tamanioTexto)),
                          ),
                        ),
                        Text(' '),
                        Text(' '),
                        new Text((busqueda2==null || busqueda2== "")?"":"Buscando resultados para $busqueda2", style: TextStyle(fontSize: tamanioTexto,fontWeight: FontWeight.bold, ), textAlign: TextAlign.justify,),
                        Text(' '),
                        new Text((resultado2==null)?"":"$resultado2", style: TextStyle(fontSize: tamanioTexto), textAlign: TextAlign.justify,),
                        Text(' '),

                      ],
                    ),
                  ),
                ),
              ),
            ),

            //3

            Center(
              child: Padding(
                padding: EdgeInsets.all(20),
                child: SingleChildScrollView(
                  child:
                  Form(
                    key:_formKey3,
                    child: Column(
                      children: <Widget>[
                        Text(' '),
                        Text(' '),
                        new Image.asset(
                          'assets/image/search.png' ,width: 70,height: 70,
                        ),
                        Text(' '),
                        Text(' '),
                        Text(
                          '  Busca información sobre el cuidado de tu hijo ',
                          style: TextStyle(
                              color: Colors.black54, fontSize: 16.0),
                        ),
                        Text(' '),
                        Text(' '),
                        TextFormField(
                          controller: _c3,
                          inputFormatters: [
                            LengthLimitingTextInputFormatter(50),
                          ],
                          onSaved: (input){
                            busqueda3=input;
                          },
                          decoration: InputDecoration(
                            labelText: 'Consulta aquí',
                            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 50.0, 10.0),
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
                          ),
                        ),
                        SizedBox(height: 8.0),
                        Text(' '),
                        Text(' '),
                        Material(
                          borderRadius: BorderRadius.circular(20.0),
                          shadowColor: menta,
                          elevation: 5.0,
                          color: menta,
                          child: MaterialButton(
                            minWidth: 200,
                            height: 42.0,
                            onPressed:() {
                              setState(()
                              {
                                busqueda3=_c3.text;
                                _formKey3.currentState.save();

                                _buscar(context, busqueda3, 'cuidado', resultado3).then((result) {
                                  print(result);
                                  setState(() {
                                    resultado3 = result;
                                  });
                                });
                                _c3.text="";
                              });
                            },
                            child: Text('Consultar', style: TextStyle(color: Colors.white, fontSize: tamanioTexto)),
                          ),
                        ),
                        Text(' '),
                        Text(' '),
                        new Text((busqueda3==null|| busqueda3== "")?"":"Buscando resultados para $busqueda3", style: TextStyle(fontSize: tamanioTexto,fontWeight: FontWeight.bold, ), textAlign: TextAlign.justify,),
                        Text(' '),
                        new Text((resultado3==null)?"":"$resultado3", style: TextStyle(fontSize: tamanioTexto), textAlign: TextAlign.justify,),
                        Text(' '),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
        // This trailing comma makes auto-formatting nicer for build methods.
      ),
    );
  }

  @override
  void screenUpdate() {
    setState(() {});
  }

  Future<String> _buscar(context, String busqueda, String tipo, String variable)async{
    bool respuesta = await coneccionState();
    if (!respuesta){
      showAlertaError(context,'Error de conexión',"No tienes conexión en este momento, por favor espera hasta recuperarla.");
      return (' ');
    }
    else{
      if(tipo=='cuidado'){
        return fire.getInfo(busqueda, variable, 'cuidado');
      }
      else if(tipo == 'etapas') {
        return fire.getInfo(busqueda, variable, 'etapas');
      }
      else {
        return fire.getInfo(busqueda, variable, 'salud');
      }
    }


  }

}
