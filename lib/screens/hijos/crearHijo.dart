import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:bhelp/DAOS/Hijo.dart';
import 'package:bhelp/DAOS/Vacuna.dart';
import 'package:bhelp/model/database_helper.dart';
import 'package:date_format/date_format.dart';
import 'package:bhelp/main.dart';
import 'package:intl/intl.dart';
// Widgets utilizados
import 'package:bhelp/widgets/AppBar.dart' as appBar;
import 'package:bhelp/widgets/Drawer.dart' as drawer;
import 'package:bhelp/model/fireStore.dart' as remoto;
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:bhelp/widgets/Center.dart' as centro;
import 'package:bhelp/presenters/visualizaPantalla.dart';


class NinosPage extends StatefulWidget {
  NinosPage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _NinosPageState createState() => _NinosPageState();
}

class _NinosPageState extends State<NinosPage> {

  var _formKey = GlobalKey<FormState>();
  DateTime fecha;
  Hijo _data= new Hijo("","", "", 0, 0 , 0, 0, 0, new List());
  int _radioValue=0;

  void _handleRadioValueChange(int value) {

    setState(() {
      _radioValue = value;

      switch (_radioValue) {
        case 0:
          _data.sexo= 0;
          break;
        case 1:
          _data.sexo= 1;
          break;
      }
    });
  }
//TO DO no deberia crarse aqui es muy ineficiente, por cada hijo se crearioan muchos objetos.
  Vacuna vacunaHepatitis0= new Vacuna(0,"Hepatitis B",0,0);
  Vacuna vacunaNeumococo2= new Vacuna(0,"Neumococo",2,1);
  Vacuna vacunaNeumococo4= new Vacuna(0,"Neumococo",4,2);
  Vacuna vacunaInfluenza6= new Vacuna(0,"Influenza",6,1);
  Vacuna vacunaInfluenza7= new Vacuna(0,"Influenza",7,2);
  Vacuna vacunaFiebreAmarilla12= new Vacuna(0,"Fiebre Amarilla",12,1);
  Vacuna vacunaTosferina18= new Vacuna(0,"Difteria-Tosferina-Tétano",18,1);
  Vacuna vacunaTosferina60= new Vacuna(0,"Difteria-Tosferina-Tétano",60,2);

  bool _value1 = false;
  bool _value2 = false;
  bool _value3 = false;
  bool _value4 = false;
  bool _value5 = false;
  bool _value6 = false;
  bool _value7 = false;
  bool _value8 = false;

  @override
  void initState(){
    super.initState();
    _data.vacunas.add(vacunaHepatitis0.toMap());
    _data.vacunas.add(vacunaNeumococo2.toMap());
    _data.vacunas.add(vacunaNeumococo4.toMap());
    _data.vacunas.add(vacunaInfluenza6.toMap());
    _data.vacunas.add(vacunaInfluenza7.toMap());
    _data.vacunas.add(vacunaFiebreAmarilla12.toMap());
    _data.vacunas.add(vacunaTosferina18.toMap());
    _data.vacunas.add(vacunaTosferina60.toMap());
  }

  void _value1Changed(bool value) =>
      setState(() {
        _value1 = value;
        vacunaHepatitis0.aplicada= (_value1)? 1:0;
       _data.vacunas.replaceRange(0, 0, [vacunaHepatitis0.toMap()]);
  });
  void _value2Changed(bool value) =>
      setState(() {
        _value2 = value;
        vacunaNeumococo2.aplicada= (_value2)? 1:0;
        _data.vacunas.replaceRange(1, 1, [vacunaNeumococo2.toMap()]);
      });
  void _value3Changed(bool value) =>
      setState(() {
        _value3 = value;
        vacunaNeumococo4.aplicada= (_value3)? 1:0;
        _data.vacunas.replaceRange(2, 2, [vacunaNeumococo4.toMap()]);
      });
  void _value4Changed(bool value) =>
      setState(() {
        _value4 = value;
        vacunaInfluenza6.aplicada= (_value4)? 1:0;
        _data.vacunas.replaceRange(3, 3, [vacunaInfluenza6.toMap()]);
      });
  void _value5Changed(bool value) =>
      setState(() {
        _value5 = value;
        vacunaInfluenza7.aplicada= (_value5)? 1:0;
        _data.vacunas.replaceRange(4, 4, [vacunaInfluenza7.toMap()]);
      });
  void _value6Changed(bool value) =>
      setState(() {
        _value6 = value;
        vacunaFiebreAmarilla12.aplicada= (_value6)? 1:0;
        _data.vacunas.replaceRange(5, 5, [vacunaFiebreAmarilla12.toMap()]);
      });
  void _value7Changed(bool value) =>
      setState(() {
        _value7 = value;
        vacunaTosferina18.aplicada= (_value7)? 1:0;
        _data.vacunas.replaceRange(6, 6, [vacunaTosferina18.toMap()]);
      });
  void _value8Changed(bool value) =>
      setState(() {
        _value8 = value;
        vacunaTosferina60.aplicada= (_value8)? 1:0;
        _data.vacunas.replaceRange(7, 7, [vacunaTosferina60.toMap()]);

      });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: appBar.barraSuperior("Agregar hijo"),
        drawer: drawer.menu(context),
        body: Padding(
          padding: EdgeInsets.all(18.0),
          child: SingleChildScrollView(
            child: new Form(
              key: _formKey,
              child:
              Column( children: <Widget>[
                SizedBox(height: 8.0),
                new Image.asset(
                  'assets/image/niño.png' ,width: 70,height: 70,
                ),
                new TextFormField(
                  keyboardType: TextInputType.text,
                  inputFormatters: [
                    LengthLimitingTextInputFormatter(30),
                  ],
                  validator: (value) {
                    if (value.length < 3) {
                      return ('El nombre debe tener al menos 3 caracteres');
                    }
                    else if(value.contains('.') || value.contains(',') || value.contains(';') || value.contains('_') || value.contains('#') || value.contains('-') || value.contains('+') || value.contains('*')|| value.contains('/')){

                      return ('Ingrese un nombre válido. No se permiten caracteres especiales.');
                    }
                    else if(value.isEmpty){

                      return ('Ingrese un nombre');
                    }
                  },
                  onSaved: (value) {
                    _data.nombre = value;
                  },
                  decoration: InputDecoration(
                    icon: Icon(Icons.child_care, color: menta),
                    labelText: 'Nombre',
                  ),

                ),

                DateTimePickerFormField(
                  decoration:InputDecoration(
                    icon:Icon(Icons.cake, color: menta),
                    labelText: 'Fecha de nacimiento',
                  ),
                  inputType: InputType.date,
                  format: DateFormat("d/MM/y"),
                  validator: (input){
                    if(input== null){
                      return'Ingrese una fecha';
                    }
                    else if(input.isAfter(DateTime.now())){
                      return 'La fecha ingresada no ha ocurrido';
                    }
                  },
                  onSaved: (value) {
                    if(value != null){
                      fecha= value;
                      _data.fecha = formatDate(
                          value, [dd, '-', mm, '-', yyyy, ]);
                    }
                  },
                  editable: true,
                ),

                new TextFormField(
                  keyboardType: TextInputType.number,
                  inputFormatters: [
                    LengthLimitingTextInputFormatter(5),
                    WhitelistingTextInputFormatter.digitsOnly,
                  ],

                  validator: (value) {
                    if (value.length > 5 || value.length<2) {
                      return ('Ingrese un peso válido');
                    }
                    else if(value.contains('.') || value.contains(',') || value.contains(';') || value.contains('_') || value.contains('#') || value.contains('-') || value.contains('+') || value.contains('*')|| value.contains('/')){

                      return ('Ingrese un peso válido.');
                    }
                    else if(value.isEmpty){
                      return ('Ingrese un valor para el peso');
                    }
                  },
                  onSaved: (value) {
                    if(value.isNotEmpty) {
                      _data.peso = int.parse(value);
                    }
                  },
                  decoration: InputDecoration(
                    icon: Icon(Icons.assignment_late, color: menta),
                    labelText: 'Peso actual (gramos)',
                  ),

                ),

                new TextFormField(
                  keyboardType: TextInputType.number,
                  inputFormatters: [
                    LengthLimitingTextInputFormatter(3),
                    WhitelistingTextInputFormatter.digitsOnly,
                  ],

                  validator: (value) {
                    if (value.length < 2) {
                      return ('Ingrese una altura válida');
                    }
                    else if(value.contains('.') || value.contains(',') || value.contains(';') || value.contains('_') || value.contains('#') || value.contains('-') || value.contains('+') || value.contains('*')|| value.contains('/')){

                      return ('Ingrese una altura válida.');
                    }
                    else if(value.isEmpty){
                      return ('Ingrese un valor para la altura');
                    }

                  },
                  onSaved: (value) {
                    if(value.isNotEmpty) {
                      _data.altura = int.parse(value);
                    }
                  },
                  decoration: InputDecoration(
                    icon: Icon(Icons.accessibility_new, color: menta),
                    labelText: 'Altura actual (cm)',
                  ),

                ),
                SizedBox(height: 8.0),
                Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Icon(Icons.account_circle, color: menta),
                      Text('     Género',
                        style: TextStyle(fontSize: tamanioTexto,color: gris),
                      ),
                    ]
                ),
                new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Radio(
                      value: 1,
                      activeColor: menta,
                      groupValue: _radioValue,
                      onChanged: _handleRadioValueChange,
                    ),
                    new Text(
                      'Femenino',
                      style: new TextStyle(fontSize: 16.0),
                    ),
                    new Radio(
                      value: 0,
                      activeColor: menta,
                      groupValue: _radioValue,
                      onChanged: _handleRadioValueChange,
                    ),
                    new Text(
                      'Masculino',
                      style: new TextStyle(
                        fontSize: 16.0,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 8.0),

                Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Icon(Icons.add_box, color: menta),
                      Text('     Vacunas',
                        style: TextStyle(fontSize: tamanioTexto,color: gris),
                      ),
                    ]
                ),
                Text(' '),
                Text('Selecciona las vacunas que ya aplicaste a tu hijo',
                    style: TextStyle(fontSize: 15,color: gris)),
                Text(' '),Text(' '),
                Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Checkbox(
                          value: _value1, onChanged: _value1Changed, activeColor: menta,
                      ),
                      Expanded(child: Text('Hepatitis B',
                        style: TextStyle(fontSize: tamanioTexto,color: gris),
                      ),),

                    ]
                ),



                Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Checkbox(
                          value: _value2, onChanged: _value2Changed, activeColor: menta,

                      ),
                      Expanded(child: Text('Neumococo (Primera dosis)',
                        style: TextStyle(fontSize: tamanioTexto,color: gris),
                      ),),

                    ]
                ),




                Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Checkbox(
                        value: _value3, onChanged: _value3Changed, activeColor: menta,

                      ),
                      Expanded(child: Text('Neumococo (Segunda dosis)',
                        style: TextStyle(fontSize: tamanioTexto,color: gris),
                      ),),

                    ]
                ),

                Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Checkbox(
                        value: _value4, onChanged: _value4Changed, activeColor: menta,

                      ),
                      Expanded(child: Text('Influenza (Primera dosis)',
                        style: TextStyle(fontSize: tamanioTexto,color: gris),
                      ),),

                    ]
                ),

                Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Checkbox(
                        value: _value5, onChanged: _value5Changed, activeColor: menta,

                      ),
                      Expanded(child: Text('Influenza (Segunda dosis)',
                        style: TextStyle(fontSize: tamanioTexto,color: gris),
                      ),),

                    ]
                ),

                Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Checkbox(
                        value: _value6, onChanged: _value6Changed, activeColor: menta,

                      ),
                      Expanded(child: Text('Fiebre amarilla',
                        style: TextStyle(fontSize: tamanioTexto,color: gris),
                      ),),

                    ]
                ),

                Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Checkbox(
                        value: _value7, onChanged: _value7Changed, activeColor: menta,

                      ),
                      Expanded(child: Text('Difteria-Tosferina (Primera dosis)',
                        style: TextStyle(fontSize: tamanioTexto,color: gris),
                      ),),
                    ]
                ),


                Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Checkbox(
                        value: _value8, onChanged: _value8Changed, activeColor: menta,

                      ),
                      Expanded(child: Text('Difteria-Tosferina (Segunda dosis)',
                        style: TextStyle(fontSize: tamanioTexto,color: gris),
                      ),),
                    ]
                ),

                Text(' '),Text(' '),
                Material(
                  borderRadius: BorderRadius.circular(20.0),
                  shadowColor: menta,
                  elevation: 5.0,
                  color: menta,
                  child: MaterialButton(
                    minWidth: 1000,
                    height: 42.0,
                    onPressed:() {
                      _save(context);
                    },
                    child: Text('Continuar', style: TextStyle(color: Colors.white, fontSize: tamanioTexto)),
                  ),
                ),


              ],

              ),
            ),
          ),

        )
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  Future addRecord() async {
    var db = new DatabaseHelper();
    var fechaActual= DateTime.now();
    int edad=0;
    if(fechaActual.year>fecha.year) {
      if (fechaActual.month > fecha.month) {
        edad = ((fechaActual.year - fecha.year) * 12) +
            (fechaActual.month - fecha.month);
        if (fechaActual.day < fecha.day)
          edad -= 1;
      }
      else {
        edad = ((fechaActual.year - fecha.year - 1) * 12) +
            (12 - fecha.month + fechaActual.month);
        if (fechaActual.day < fecha.day)
          edad -= 1;
      }
    }
    else {
      if (fechaActual.month > fecha.month) {
        edad = (fechaActual.month - fecha.month);
        if (fechaActual.day < fecha.day)
          edad -= 1;
      }
    }
    _data.edad=edad;

    var hijo = new Hijo(_data.nombre+"-"+_data.fecha,
        _data.nombre, _data.fecha,_data.edad, _data.peso, _data.altura, _data.sexo,
        _data.compartido, _data.vacunas);
    await db.saveHijo(hijo);
    remoto.crearHijo(hijo);
  }


  Future<void> _save(context) async{
    final formState = _formKey.currentState;
    formState.save();
    if(formState.validate()){
      int numHijos=sharedPreferencesFull.getInt('hijos');
      sharedPreferencesFull.setInt('hijos', numHijos+1);
      try{
        addRecord();
        Navigator.pop(context);
      }catch(e){
        print('ERROR');
        //print(e.message);
      }
    }
  }
}