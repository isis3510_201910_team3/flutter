import 'package:flutter/material.dart';
import 'package:bhelp/main.dart';
import 'package:bhelp/DAOS/Hijo.dart';
import 'package:bhelp/model/database_helper.dart';
import 'package:flutter/services.dart';
import 'presentacion_hijos.dart' as PH;
// Widgets utilizados
import 'package:bhelp/widgets/AppBar.dart' as appBar;
import 'package:bhelp/widgets/Drawer.dart' as drawer;
import 'package:intl/intl.dart';
import 'package:bhelp/presenters/visualizaPantalla.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:bhelp/model/fireStore.dart' as remoto;

import 'package:date_format/date_format.dart';

class NinosPage extends StatefulWidget {
  Hijo hijoActual;
  VisualizaPantalla visualizacionPantalla;

  NinosPage(this.hijoActual, this.visualizacionPantalla);

  @override
  _NinosPageState createState() => _NinosPageState(hijoActual, visualizacionPantalla);
}

class _NinosPageState extends State<NinosPage> {

  Hijo hijoActual;
  VisualizaPantalla visualizacionPantalla;
  var fecha;

  _NinosPageState(this.hijoActual,this.visualizacionPantalla);

  var _formKey = GlobalKey<FormState>();
  Hijo _data= new Hijo("","", "", 0, 0, 0 , 0, 0, new List());


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: appBar.barraSuperior('Modifica los datos de tu hijo'),
        drawer: drawer.menu(context),
        body: Padding(
          padding: EdgeInsets.all(18.0),
          child: SingleChildScrollView(
            child: new Form(
              key: _formKey,
              child:
              Column(
                children: <Widget>[
                  Text(' '),
                  Text(' '),
                new Image.asset(
                  'assets/image/niño.png' ,width: 70,height: 70,
                ),
                new Text(
                  '   ',
                  style: TextStyle(
                      color: Colors.white, fontSize: 5.0),
                ),
                new TextFormField(
                  keyboardType: TextInputType.text,
                  inputFormatters: [
                    LengthLimitingTextInputFormatter(30),
                  ],
                  validator: (value) {
                    if (value.length < 3) {
                      return ('El nombre debe tener al menos 3 caracteres');
                    }
                    else if(value.contains('.') || value.contains(',') || value.contains(';') || value.contains('_') || value.contains('#') || value.contains('-') || value.contains('+') || value.contains('*')|| value.contains('/')){

                      return ('Ingrese un nombre válido. No se permiten caracteres especiales.');
                    }
                    else if(value.isEmpty){

                      return ('Ingrese un nombre');
                    }
                  },
                  onSaved: (value) {
                    _data.nombre = value;
                  },
                  decoration: InputDecoration(
                    icon: Icon(Icons.child_care, color: menta),
                    labelText: 'Nombre',
                  ),

                ),

                DateTimePickerFormField(
                  decoration:InputDecoration(
                    icon:Icon(Icons.cake, color: menta),
                    labelText: 'Fecha de nacimiento',
                  ),
                  inputType: InputType.date,
                  format: DateFormat("d/MM/y"),
                  validator: (input){
                    if(input== null){
                      return'Ingrese una fecha';
                    }
                    else if(input.isAfter(DateTime.now())){
                      return 'La fecha ingresada no ha ocurrido';
                    }
                  },
                  onSaved: (value) {
                    if(value != null){
                      fecha= value;
                      _data.fecha = formatDate(
                          value, [dd, '-', mm, '-', yyyy, ]);
                    }
                  },
                  editable: true,
                ),

                new TextFormField(
                  keyboardType: TextInputType.number,
                  inputFormatters: [
                    LengthLimitingTextInputFormatter(5),
                  ],

                  validator: (value) {
                    if (value.length > 5 || value.length<2) {
                      return ('Ingrese un peso válido');
                    }
                    else if(value.contains('.') || value.contains(',') || value.contains(';') || value.contains('_') || value.contains('#') || value.contains('-') || value.contains('+') || value.contains('*')|| value.contains('/')){

                      return ('Ingrese un peso válido.');
                    }
                    else if(value.isEmpty){
                      return ('Ingrese un valor para el peso');
                    }
                  },
                  onSaved: (value) {
                    if(value.isNotEmpty) {
                      _data.peso = int.parse(value);
                    }
                  },
                  decoration: InputDecoration(
                    icon: Icon(Icons.assignment_late, color: menta),
                    labelText: 'Peso (gr)',
                  ),

                ),

                new TextFormField(
                  keyboardType: TextInputType.number,
                  inputFormatters: [
                    LengthLimitingTextInputFormatter(3),
                  ],

                  validator: (value) {
                    if (value.length < 2) {
                      return ('Ingrese una altura válida');
                    }
                    else if(value.contains('.') || value.contains(',') || value.contains(';') || value.contains('_') || value.contains('#') || value.contains('-') || value.contains('+') || value.contains('*')|| value.contains('/')){

                      return ('Ingrese una altura válida.');
                    }
                    else if(value.isEmpty){
                      return ('Ingrese un valor para la altura');
                    }
                  },
                  onSaved: (value) {
                    if(value.isNotEmpty) {
                      _data.altura = int.parse(value);
                    }
                  },
                  decoration: InputDecoration(
                    icon: Icon(Icons.accessibility_new, color: menta),
                    labelText: 'Altura (cm)',
                  ),

                ),
                Text("       "),

                Text("       "),
                Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Icon(Icons.account_circle, color: menta),
                      Text('     Género',
                        style: TextStyle(fontSize: tamanioTexto,color: gris),
                      ),
                    ]
                ),
                new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Radio(
                      value: 1,
                      activeColor: menta,
                      groupValue: _radioValue,
                      onChanged: _handleRadioValueChange,
                    ),
                    new Text(
                      'Femenino',
                      style: new TextStyle(fontSize: 16.0),
                    ),
                    new Radio(
                      value: 0,
                      activeColor: menta,
                      groupValue: _radioValue,
                      onChanged: _handleRadioValueChange,
                    ),
                    new Text(
                      'Masculino',
                      style: new TextStyle(
                        fontSize: 16.0,
                      ),
                    ),
                  ],
                ),

                new Text(
                    "       "
                ),new Text(
                    "       "
                ),

                Material(
                  borderRadius: BorderRadius.circular(20.0),
                  shadowColor: menta,
                  elevation: 5.0,
                  color: menta,
                  child: MaterialButton(
                    minWidth: 1000,
                    height: 42.0,
                    onPressed:() {
                      _save(context);
                    },
                    child: Text('Continuar', style: TextStyle(color: Colors.white, fontSize: tamanioTexto)),
                  ),
                ),


              ],

              ),
            ),
          ),

        )
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  double _result = 0.0;
  double _resultEdad = 0.0;
  int _radioValue=0;
  int _radioValueEdad=0;

  @override
  void initState() {
    setState(() {
      _radioValue = hijoActual.sexo;
    });
    super.initState();
  }

  void _handleRadioValueChange(int value) {

    setState(() {

      _radioValue = value;

      switch (_radioValue) {
        case 0:
          _result = 0;
          _data.sexo= 0;
          break;
      case 1:
        _result = 1;
        _data.sexo= 1;
        break;

    }
    });
  }


  Future addEditedRecord() async {
    var db = new DatabaseHelper();
    var fechaActual= DateTime.now();


    /*if (fechaActual.difference(fecha).inDays>365) {
      _data.edad= (fechaActual.difference(fecha).inDays/365).floor();
      _data.esBebe = ' años';
    }
    else {
      print('Se deben calcular los meses');
      if(fechaActual.difference(fecha).inDays<31)
      {
        _data.edad= (fechaActual.difference(fecha).inDays/1).floor();
        _data.esBebe = ' días';
      }
      else
      {
        _data.edad= (fechaActual.difference(fecha).inDays/31).floor();
        if(_data.edad ==1) {
          _data.esBebe = ' mes';
        }
        else
          {
            _data.esBebe = ' meses';
          }
      }
    }*/
    var hijo = new Hijo(hijoActual.id,
        _data.nombre, _data.fecha, _data.edad,_data.peso, _data.altura, _data.sexo,
        _data.compartido, hijoActual.vacunas);


    hijo.id = hijoActual.id;
    await db.updateHijo(hijo);
    await remoto.updateHijoFires(hijoActual, hijo);

    visualizacionPantalla.updateScreen();
  }

  Future<void> _save(context) async{
    final formState = _formKey.currentState;
    formState.save();
    if(formState.validate()){
      try{
        addEditedRecord();
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => PH.HijosVistaPage()),
        );

      }catch(e){
        print('ERROR');
        print(e.message);
      }
    }
  }
}