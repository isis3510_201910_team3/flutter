import 'package:flutter/material.dart';
import 'package:bhelp/DAOS/Hijo.dart';
import 'package:bhelp/screens/hijos/vistaDetalladaHijo.dart';
import 'package:bhelp/presenters/visualizaPantalla.dart';

// Widgets utilizados
import 'package:bhelp/widgets/Drawer.dart' as drawer;


class AgendaPage extends StatefulWidget {
  Hijo hijoActual;
  VisualizaPantalla visualizacionPantalla;

  AgendaPage(this.hijoActual, this.visualizacionPantalla);

  @override
  AgendaPageState createState() => AgendaPageState(hijoActual,visualizacionPantalla);
}


class AgendaPageState extends State<AgendaPage> implements Vista{

  Hijo hijoActual;
  VisualizaPantalla visualizacionPantalla;
  AgendaPageState(this.hijoActual, this.visualizacionPantalla);
  List tareas = [];
  VisualizaPantalla homePresenter;

  @override
  void initState() {
    super.initState();
    homePresenter = new VisualizaPantalla(this);
  }

  displayRecord() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Información de tu hijo'),
        ),
        drawer: drawer.menu(context),
        body: TabBarView(
          children: [
            Center(
              child: new FutureBuilder<Hijo>(
                builder: (context, snapshot) {
                  return new VistaHijoDetallado(hijoActual,homePresenter);
                },
              ),
            ),
          ],
        ), // This trailing comma makes auto-formatting nicer for build method
      ),
    );
  }

  @override
  void screenUpdate() {
    setState(() {});
  }

}