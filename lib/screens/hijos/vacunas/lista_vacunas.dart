import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:bhelp/DAOS/Vacuna.dart';
import 'package:bhelp/DAOS/Hijo.dart';
import 'package:bhelp/presenters/visualizaPantalla.dart';
import 'package:bhelp/widgets/EditDialog.dart' as dialogoEditar;

var idActual;
var vacuna= new Vacuna(0,"",0,0);

class VacunasList extends StatelessWidget {
  List item;
  VisualizaPantalla visualizacionPantalla;
  Hijo hijoActual;


  VacunasList(
      List this.item,
      VisualizaPantalla this.visualizacionPantalla,
      Hijo this.hijoActual,

      {
        Key key,
      }) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return new ListView.builder(
        itemCount: item == null ? 0 : (item.length),
        itemBuilder: (BuildContext context, int index) {
           return new Card(
            child: new Container(
                child: new GestureDetector(
                  child: new Row(
                    children: <Widget>[
                      new Image.asset(
                        'assets/image/injection.png',
                        width: 67,height: 67,
                      ),
                      new Expanded(
                        child: new Padding(
                          padding: EdgeInsets.all(8.0),
                          child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Text(
                                (item[index].nombre),
                                // set some style to text
                                style: new TextStyle(
                                    fontSize: 16.0,
                                    color: Colors.black
                                ),
                              ),
                              SizedBox(height: 8.0),
                              Text(
                                (item[index].aplicada==1)?'Ya le aplicaste esta vacuna a tu hijo':'Aplícala a tu hijo a los ' + (item[index].edad).toString() + ' meses',
                                // set some style to text
                                style: new TextStyle(
                                    fontSize: 15.0, color: Colors.black54),
                              ),
                            ],
                          ),
                        ),
                      ),

                      //IconButton(
                        //icon: (item[index].aplicada==1)?const Icon(Icons.check_box, color: Colors.blueGrey):const Icon(Icons.check_box_outline_blank, color: Colors.blueGrey),
                        //onPressed: () {
                          //(item[index].aplicada==0)? (
                          //dialogoEditar.editDialog(context, 'assets/image/injection.png', 'Vacunación realizada', '¿Desea que establecer que la vacuna fue colocada a su hijo?')
                            //  .then((value) {
                            //if(value) {
                              //var antigua= item[index];
                              //var nueva= new Vacuna(1, antigua.nombre, antigua.edad, antigua.dosis);

                              //item.replaceRange(index, index, [nueva.toMap()]);


//                              var hijoNuevo = new Hijo(hijoActual.nombre+"-"+hijoActual.fecha,
//                                  hijoActual.nombre, hijoActual.fecha,hijoActual.edad, hijoActual.peso, hijoActual.altura, hijoActual.sexo,
  //                                hijoActual.compartido, item);
//
  //                            visualizacionPantalla.updateVacunaHijo(hijoActual, hijoNuevo);
    //                          visualizacionPantalla.updateScreen();
      //                      }
        //                  })):null;
          //                },
            //          )


                    ],
                  ),
                ),
                padding: const EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0)),
          );
        });
  }



  displayRecord() {
    visualizacionPantalla.updateScreen();
  }
}