import 'package:flutter/material.dart';
import 'package:bhelp/DAOS/Vacuna.dart' as Vacunas;
import 'package:bhelp/DAOS/Hijo.dart';
import 'package:bhelp/widgets/Drawer.dart' as drawer;
import 'package:bhelp/presenters/visualizaPantalla.dart';
import 'package:bhelp/screens/hijos/vacunas/lista_vacunas.dart';
import 'package:bhelp/model/fireStore.dart' as fire;
import 'package:bhelp/model/conectivity.dart';

class VacunasVistaPage extends StatefulWidget {

  Hijo hijoActual;

  VacunasVistaPage(this.hijoActual);

  @override
  _VacunasVistaPageState createState() => _VacunasVistaPageState(hijoActual);

}


class _VacunasVistaPageState extends State<VacunasVistaPage> implements Vista{

  Hijo hijoActual;
  _VacunasVistaPageState(this.hijoActual);

  List vacunas = [];
  VisualizaPantalla homePresenter;


  @override
  void initState() {
    super.initState();
    homePresenter = new VisualizaPantalla(this);
  }

  displayRecord() {
    setState(() {});
  }



  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(

          title: Text('Vacunas para tu hijo'),
        ),
        drawer: drawer.menu(context),
        body: TabBarView(
          children: [
            Center(
              child: new FutureBuilder<List>(
              future: fire.getTodasVacunasHijo(hijoActual.id),
              builder: (context, snapshot) {
                if (snapshot.hasError) print(snapshot.error);
                var data = snapshot.data;
                if(data!=null) {
                  return (data.length > 0)
                      ? new VacunasList(data, homePresenter, hijoActual)
                      : new Text('No hay información de vacunas para este niño');
                }
                else
                {
                  return new CircularProgressIndicator();
                }
              },
            ),

            ),
          ],
        ), // This trailing comma makes auto-formatting nicer for build methods.

      ),

    );

  }

  @override
  void screenUpdate() {
    setState(() {});
  }

  Future<List> getVacunas( ) async{
    bool respuesta = await coneccionState();
    if(respuesta){
      return fire.getTodasVacunas();
    }
    else{

      return new List();
    }
  }
}
