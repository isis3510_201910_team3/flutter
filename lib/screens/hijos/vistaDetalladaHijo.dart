import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:bhelp/DAOS/Hijo.dart';
import 'package:bhelp/presenters/visualizaPantalla.dart';
import 'package:bhelp/screens/hijos/vacunas/presentacion_vacunas.dart' as PH;
import 'package:bhelp/main.dart';
import 'package:bhelp/widgets/Center.dart' as centro;

var idActual;

class VistaHijoDetallado extends StatelessWidget {
  Hijo item;
  VisualizaPantalla visualizacionPantalla;

  VistaHijoDetallado(
      Hijo this.item,
      VisualizaPantalla this.visualizacionPantalla, {
        Key key,
      }) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return new Align(

      alignment: Alignment.topCenter,

      child: SingleChildScrollView(


        child: new Column(
          children: <Widget>[
            SizedBox(height: 16.0),
            new Image.asset(
              ( (item.sexo==1)? ('assets/image/nina.png'): ('assets/image/nino.png')),
              width: 100,height: 100,
            ),
            new Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new Text(' '),
                new Text(
                  (item.nombre!=null)? ( "Nombre: " + (item.nombre)): ("Nombre" ),
                  // set some style to text
                  style: new TextStyle(
                      fontSize: 16.0,
                      color: Colors.black
                  ),
                ),
                new Text(
                  (item.fecha!=null)? ( "Edad: "+ edad(item.fecha)): ("Edad" ),
                  // set some style to text
                  style: new TextStyle(
                      fontSize: 15.0, color: Colors.black54
                  ),
                ),
                new Text(
                  (item.peso!=null)? ( "Peso: " + (item.peso).toString()): ("Peso"),
                  // set some style to text
                  style: new TextStyle(
                      fontSize: 15.0, color: Colors.black54
                  ),
                ),
                new Text(
                  (item.altura!=null)? ( "Altura: " + (item.altura).toString()): ("Altura"),
                  // set some style to text
                  style: new TextStyle(
                      fontSize: 15.0, color: Colors.black54
                  ),
                ),
                new Text(
                  (item.sexo!=null)? ( "Sexo: " + ((item.sexo==1)?"Femenino":"Masculino")): ("Sexo"),
                  // set some style to text
                  style: new TextStyle(
                      fontSize: 15.0, color: Colors.black54
                  ),
                ),
                SizedBox(height: 16.0),

                Material(
                  borderRadius: BorderRadius.circular(20.0),
                  shadowColor: menta,
                  elevation: 5.0,
                  color: menta,
                  child: MaterialButton(
                    minWidth: 100,
                    height: 42.0,
                    onPressed:() {

                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => PH.VacunasVistaPage(item)),
                      );

                    },
                    child: Text('Ver información de vacunas', style: TextStyle(color: Colors.white, fontSize: tamanioTexto)),
                  ),
                ),
                SizedBox(height: 16.0),
                Material(
                  borderRadius: BorderRadius.circular(0),
                  shadowColor: menta,
                  elevation: 0.0,
                  color: rosa,
                  child: MaterialButton(
                    minWidth: 1000,
                    height: 30.0,
                    onPressed:() {
                    },
                    child: Text('Tareas prioritarias', style: TextStyle(color: Colors.white, fontSize: tamanioTexto)),
                  ),
                ),

                centro.listaTareasPrioridadAlta(visualizacionPantalla, 'Alta', 'assets/image/notification.png', 'Aún no tienes tareas prioritarias', item.id),


              ],
            ),

          ],

        ),),
    );
  }

  displayRecord() {
    visualizacionPantalla.updateScreen();
  }

  String edad(String fechaCumple) {
    List<String> fecha=fechaCumple.split("-");
    int dia=int.parse(fecha[0]);
    int mes=int.parse(fecha[1]);
    int anio =int.parse(fecha[2]);
    var fechaActual= DateTime.now();
    int edad=0;

    if(fechaActual.year>anio){
      if(fechaActual.month>mes){
        edad=(fechaActual.year-anio);
        if(edad==1)
          return edad.toString()+" año";
        return edad.toString()+" años";
      }
      else{
        edad=(12-mes+fechaActual.month);
        if(fechaActual.day<dia)
          edad-=1;
        if(edad==1)
          return edad.toString()+"mes";
        return edad.toString()+" meses";
      }
    }
    else{
      if(fechaActual.month>mes){
        edad=(fechaActual.month-mes);
        if(fechaActual.day<dia)
          edad-=1;
        if(edad==1)
          return edad.toString()+" mes";
        return edad.toString()+" meses";
      }
      else{
        edad=fechaActual.day-dia;
        if(edad==1)
          return edad.toString()+" día";
        return edad.toString()+" días";
      }
    }
  }

}
