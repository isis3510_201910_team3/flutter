import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:bhelp/DAOS/Hijo.dart';
import 'package:bhelp/presenters/visualizaPantalla.dart';
import 'detalleHijo.dart' as Detalle;
import 'package:bhelp/screens/hijos/edicionNiño.dart' as Edicion;
import 'package:bhelp/widgets/DeleteDialog.dart' as dialogoEliminar;

var idActual;
var hijo= new Hijo("","","", 0,0,0,0,0, new List());

class HijosList extends StatelessWidget {
  List<Hijo> item;
  VisualizaPantalla visualizacionPantalla;

  HijosList(
      List<Hijo> this.item,
      VisualizaPantalla this.visualizacionPantalla, {
        Key key,
      }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new ListView.builder(
        itemCount: item == null ? 0 : item.length,
        itemBuilder: (BuildContext context, int index) {
          idActual=index;
          hijo=item[index];
          return new Card(
            child: new Container(
                child: new GestureDetector(
                  onTap: () => _openDialogDetailItem(item[index], context),
                  child: new Row(
                    children: <Widget>[
                      new Image.asset(
                        ( (item[index].sexo==1)? ('assets/image/nina.png'): ('assets/image/nino.png')),
                        width: 67,height: 67,
                      ),
                      new Expanded(
                        child: new Padding(
                          padding: EdgeInsets.all(8.0),
                          child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Text(
                                (item[index].nombre!=null)? ( item[index].nombre): ("Nombre" ),
                                // set some style to text
                                style: new TextStyle(
                                    fontSize: 16.0,
                                    color: Colors.black
                                ),
                              ),
                              SizedBox(height: 8.0),
                              new Text(
                                (item[index].fecha!=null)? ( "Edad: " + edad(item[index].fecha)): ("Edad: Desconocida" ),
                                // set some style to text
                                style: new TextStyle(
                                    fontSize: 15.0, color: Colors.black54),
                              ),
                            ],
                          ),
                        ),
                      ),
                      new Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[

                          new IconButton(
                              icon: const Icon(Icons.mode_edit,
                                  color: Colors.blueGrey),
                              onPressed: () => _openDialogEditItem(item[index], context),

                          ),

                         // new IconButton(
                           //   icon: const Icon(Icons.delete_forever,
                             //     color: Colors.blueGrey),
                              //onPressed: () {
                               // dialogoEliminar.deleteDialog(context, ( (item[index].sexo==1)? ('assets/image/nina.png'): ('assets/image/nino.png')), 'Eliminar hijo', 'Tu hijo seleccionado se eliminará permanentemente. ¿Desea continuar?').then((value) {
                                 // if(value) {
                                   // visualizacionPantalla.deleteHijo(item[index]);
                                  //}
                                //});
                              //}
                         // ),
                        ],
                      ),
                    ],
                  ),
                ),
                padding: const EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0)),
          );
        });
  }
  displayRecord() {
    visualizacionPantalla.updateScreen();
  }

  Future _openDialogDetailItem(Hijo hijoActual, BuildContext context) async {

    Hijo data = await Navigator.of(context).push(
        new MaterialPageRoute<Hijo>(
            builder: (BuildContext context) {
              return new Detalle.AgendaPage(hijoActual, visualizacionPantalla);
            },
            fullscreenDialog: true));
    visualizacionPantalla.updateScreen();

  }

  Future _openDialogEditItem(Hijo hijo, BuildContext context) async {
    Hijo data =
    await Navigator.of(context).push(new MaterialPageRoute<Hijo>(
        builder: (BuildContext context) {
          return new Edicion.NinosPage(hijo,visualizacionPantalla);
        },
        fullscreenDialog: true));
    visualizacionPantalla.updateScreen();
  }

  String edad(String fechaCumple) {
    List<String> fecha=fechaCumple.split("-");
    int dia=int.parse(fecha[0]);
    int mes=int.parse(fecha[1]);
    int anio =int.parse(fecha[2]);
    var fechaActual= DateTime.now();
    int edad=0;

    if(fechaActual.year>anio){
      if(fechaActual.month>mes){
        edad=(fechaActual.year-anio);
        if(edad==1)
          return edad.toString()+" año";
        return edad.toString()+" años";
      }
      else{
        edad=(12-mes+fechaActual.month);
        if(fechaActual.day<dia)
          edad-=1;
        if(edad==1)
          return edad.toString()+"mes";
        return edad.toString()+" meses";
      }
    }
    else{
      if(fechaActual.month>mes){
        edad=(fechaActual.month-mes);
        if(fechaActual.day<dia)
          edad-=1;
        if(edad==1)
          return edad.toString()+" mes";
        return edad.toString()+" meses";
      }
      else{
        edad=fechaActual.day-dia;
        if(edad==1)
          return edad.toString()+" día";
        return edad.toString()+" días";
      }
    }
  }
}