import 'package:flutter/material.dart';
import 'package:bhelp/main.dart';
import 'crearHijo.dart' as Hijo;
import 'package:bhelp/DAOS/Hijo.dart' as HijosRegistrados;
import 'package:bhelp/widgets/Drawer.dart' as drawer;
import 'package:bhelp/presenters/visualizaPantalla.dart';
import 'package:bhelp/screens/hijos/lista_hijos.dart';
class HijosVistaPage extends StatefulWidget {
  HijosVistaPage({Key key, this.title5}) : super(key: key);

  final String title5;

  @override
  _HijosVistaPageState createState() => _HijosVistaPageState();
}


class _HijosVistaPageState extends State<HijosVistaPage> implements Vista{

  List hijos = [];
  VisualizaPantalla homePresenter;

  @override
  void initState() {
    super.initState();
    homePresenter = new VisualizaPantalla(this);
  }

  displayRecord() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(

          title: Text('BHelp'),
        ),
        drawer: drawer.menu(context),
        body: TabBarView(
          children: [
            Center(
              child: new FutureBuilder<List<HijosRegistrados.Hijo>>(
              future: homePresenter.getHijos(),
              builder: (context, snapshot) {
                if (snapshot.hasError) print(snapshot.error);
                var data = snapshot.data;
                if(data!=null) {
                  return (data.length > 0)
                      ? new HijosList(data, homePresenter)
                      : new Padding(padding: EdgeInsets.fromLTRB(10, 30, 20, 20),
                    child: new SingleChildScrollView(
                      child: new Column(
                        children: <Widget>[
                          new Image.asset(
                            'assets/image/baby.png',
                            width: 187,
                            height: 187,
                          ),
                          new Text('  '),
                          new Text('Aún no tienes hijos registrados', style: TextStyle(fontSize: 15.0),),
                          new Text('¡Ingresa uno!', style: TextStyle(fontSize: 15.0, color: Colors.redAccent, fontWeight: FontWeight.bold ),),
                        ],
                      ),
                    )

                  );
                }
                else
                {
                  return new CircularProgressIndicator();
                }
              },
            ),

            ),
          ],
        ),

        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Hijo.NinosPage()),
            );
          },
          tooltip: 'Hijos',
          child: new Icon(Icons.add),
          backgroundColor: menta,
        ), // This trailing comma makes auto-formatting nicer for build methods.

      ),

    );

  }

  @override
  void screenUpdate() {
    setState(() {});
  }
}
