import 'dart:math';
import 'package:flutter/material.dart';

class PuzzlePiece extends StatefulWidget {
  final Image imagenSeleccionada;
  final Size tamanio;
  final int fila;
  final int columna;
  final int maxFilas;
  final int maxCol;
  final Function traerAlFrente;
  final Function enviarAlFondo;

  PuzzlePiece(
      {Key key,
        @required this.imagenSeleccionada,
        @required this.tamanio,
        @required this.fila,
        @required this.columna,
        @required this.maxFilas,
        @required this.maxCol,
        @required this.traerAlFrente,
        @required this.enviarAlFondo})
      : super(key: key);

  @override
  PuzzlePiezaState createState() {
    return new PuzzlePiezaState();
  }
}

class PuzzlePiezaState extends State<PuzzlePiece> {
  double top;
  double left;
  bool isMovible = true;

  @override
  Widget build(BuildContext context) {
    final anchoImagen = MediaQuery.of(context).size.width;
    final altoImagen = MediaQuery.of(context).size.height *
        MediaQuery.of(context).size.width /
        widget.tamanio.width;
    final anchoPieza = anchoImagen / widget.maxCol;
    final altoPieza = altoImagen / widget.maxFilas;

    if (top == null) {
      top = Random().nextInt((altoImagen - altoPieza).ceil()).toDouble();
      top -= widget.fila * altoPieza;
    }
    if (left == null) {
      left = Random().nextInt((anchoImagen - anchoPieza).ceil()).toDouble();
      left -= widget.columna * anchoPieza;
    }

    return Positioned(
      top: top,
      left: left,
      width: anchoImagen,
      child: GestureDetector(
        onTap: () {
          if (isMovible) {
            widget.traerAlFrente(widget);
          }
        },
        onPanStart: (_) {
          if (isMovible) {
            widget.traerAlFrente(widget);
          }
        },
        onPanUpdate: (dragUpdateDetails) {
          if (isMovible) {
            setState(() {
              top += dragUpdateDetails.delta.dy;
              left += dragUpdateDetails.delta.dx;

              if (-10 < top && top < 10 && -10 < left && left < 10) {
                top = 0;
                left = 0;
                isMovible = false;
                widget.enviarAlFondo(widget);
              }
            });
          }
        },
        child: ClipPath(
          child: CustomPaint(
              foregroundPainter: TrazadorDeBorde(
                  widget.fila, widget.columna, widget.maxFilas, widget.maxCol),
              child: widget.imagenSeleccionada),
          clipper: PiezaClipper(
              widget.fila, widget.columna, widget.maxFilas, widget.maxCol),
        ),
      ),
    );
  }
}

class PiezaClipper extends CustomClipper<Path> {
  final int fila;
  final int col;
  final int maxFila;
  final int maxCol;

  PiezaClipper(this.fila, this.col, this.maxFila, this.maxCol);

  @override
  Path getClip(Size size) {
    return obtenerPathPieza(size, fila, col, maxFila, maxCol);
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}

class TrazadorDeBorde extends CustomPainter {
  final int fila;
  final int col;
  final int maxFila;
  final int maxCol;

  TrazadorDeBorde(this.fila, this.col, this.maxFila, this.maxCol);

  @override
  void paint(Canvas canvas, Size size) {
    final Paint paint = Paint()
      ..color = Color(0x80FFFFFF)
      ..style = PaintingStyle.stroke
      ..strokeWidth = 1.0;

    canvas.drawPath(obtenerPathPieza(size, fila, col, maxFila, maxCol), paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}


Path obtenerPathPieza(Size tam, int fila, int col, int maxFila, int maxCol) {
  final ancho = tam.width / maxCol;
  final alto = tam.height / maxFila;
  final offsetX = col * ancho;
  final offsetY = fila * alto;
  final bumpSize = alto / 4;

  var path = Path();
  path.moveTo(offsetX, offsetY);

  if (fila == 0) {
    // top side piece
    path.lineTo(offsetX + ancho, offsetY);
  } else {
    // top bump
    path.lineTo(offsetX + ancho / 3, offsetY);
    path.cubicTo(
        offsetX + ancho / 6,
        offsetY - bumpSize,
        offsetX + ancho / 6 * 5,
        offsetY - bumpSize,
        offsetX + ancho / 3 * 2,
        offsetY);
    path.lineTo(offsetX + ancho, offsetY);
  }

  if (col == maxCol - 1) {
    // right side piece
    path.lineTo(offsetX + ancho, offsetY + alto);
  } else {
    // right bump
    path.lineTo(offsetX + ancho, offsetY + alto / 3);
    path.cubicTo(
        offsetX + ancho - bumpSize,
        offsetY + alto / 6,
        offsetX + ancho - bumpSize,
        offsetY + alto / 6 * 5,
        offsetX + ancho,
        offsetY + alto / 3 * 2);
    path.lineTo(offsetX + ancho, offsetY + alto);
  }

  if (fila == maxFila - 1) {
    // bottom side piece
    path.lineTo(offsetX, offsetY + alto);
  } else {
    // bottom bump
    path.lineTo(offsetX + ancho / 3 * 2, offsetY + alto);
    path.cubicTo(
        offsetX + ancho / 6 * 5,
        offsetY + alto - bumpSize,
        offsetX + ancho / 6,
        offsetY + alto - bumpSize,
        offsetX + ancho / 3,
        offsetY + alto);
    path.lineTo(offsetX, offsetY + alto);
  }

  if (col == 0) {
    // left side piece
    path.close();
  } else {
    // left bump
    path.lineTo(offsetX, offsetY + alto / 3 * 2);
    path.cubicTo(
        offsetX - bumpSize,
        offsetY + alto / 6 * 5,
        offsetX - bumpSize,
        offsetY + alto / 6,
        offsetX,
        offsetY + alto / 3);
    path.close();
  }

  return path;
}