import 'dart:async';
import 'dart:io';
import 'package:bhelp/widgets/AppBar.dart' as appBar;
import 'package:bhelp/widgets/Drawer.dart' as drawer;
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import 'package:bhelp/screens/Rompecabezas/puzzle.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Desarrollo de habilidades cognitivas',
      theme: ThemeData(
          primaryColor: Color.fromARGB(255, 254, 90, 90),
          fontFamily: 'Roboto'
      ),
      home: MyHomePage(title: 'Desarrollo de habilidades cognitivas'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  final String title;
  final int filas = 3;
  final int cols = 3;

  MyHomePage({Key key, this.title}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  File imagenSeleccionada;
  List<Widget> piezas = [];

  Future getImage(ImageSource source) async {
    var image = await ImagePicker.pickImage(source: source);

    if (image != null) {
      setState(() {
        imagenSeleccionada = image;
        piezas.clear();
      });

      splitImage(Image.file(image));
    }
  }

  // we need to find out the image size, to be used in the PuzzlePiece widget
  Future<Size> getImageSize(Image image) async {
    final Completer<Size> completer = Completer<Size>();

    image.image.resolve(const ImageConfiguration()).addListener(
          (ImageInfo info, bool _) {
        completer.complete(Size(
          info.image.width.toDouble(),
          info.image.height.toDouble(),
        ));
      },
    );

    final Size imageSize = await completer.future;

    return imageSize;
  }

  void splitImage(Image image) async {
    Size imageSize = await getImageSize(image);

    for (int x = 0; x < widget.filas; x++) {
      for (int y = 0; y < widget.cols; y++) {
        setState(() {
          piezas.add(PuzzlePiece(key: GlobalKey(),
              imagenSeleccionada: image,
              tamanio: imageSize,
              fila: x,
              columna: y,
              maxFilas: widget.filas,
              maxCol: widget.cols,
              traerAlFrente: this.bringToTop,
              enviarAlFondo: this.sendToBack));
        });
      }
    }
  }

  void bringToTop(Widget widget) {
    setState(() {
      piezas.remove(widget);
      piezas.add(widget);
    });
  }

  void sendToBack(Widget widget) {
    setState(() {
      piezas.remove(widget);
      piezas.insert(0, widget);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar.barraSuperior(widget.title),
      drawer: drawer.menu(context),
      body: SafeArea(
        child: new Center(
            child: imagenSeleccionada == null
                ? new Text('Seleccione una imagen para crear un rompecabezas.')
                : Stack(children: piezas)
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.green,
        onPressed: () {
          showModalBottomSheet(context: context,
              builder: (BuildContext context) {
                return SafeArea(
                  child: new Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      new ListTile(
                        leading: new Icon(Icons.camera),
                        title: new Text('Cámara'),
                        onTap: () {
                          getImage(ImageSource.camera);
                          // this is how you dismiss the modal bottom sheet after making a choice
                          Navigator.pop(context);
                        },
                      ),
                      new ListTile(
                        leading: new Icon(Icons.image),
                        title: new Text('Galería'),
                        onTap: () {
                          getImage(ImageSource.gallery);
                          // dismiss the modal sheet
                          Navigator.pop(context);
                        },
                      ),
                    ],
                  ),
                );
              }
          );
        },
        tooltip: 'Nueva imagen',
        child: Icon(Icons.camera_enhance),
      ),
    );
  }
}