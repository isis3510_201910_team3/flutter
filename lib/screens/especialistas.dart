import 'package:flutter/material.dart';
// Widgets utilizados
import 'package:bhelp/widgets/AppBar.dart' as appBar;
import 'package:bhelp/widgets/Drawer.dart' as drawer;
import 'package:bhelp/model/fireStore.dart' as espec;

class Especialistas extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'BHelp_bebes',
      theme: ThemeData(
          primaryColor: Color.fromARGB(255, 254, 90, 90),
          fontFamily: 'Roboto'
      ),
      home: EspecialistasPage(title: 'Especialistas asociados '),
    );
  }
}
class EspecialistasPage extends StatefulWidget {
  EspecialistasPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _EspecialistasPageState createState() => _EspecialistasPageState();
}

class _EspecialistasPageState extends State<EspecialistasPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar.barraSuperior(widget.title),
      drawer: drawer.menu(context),
      body: Column(
          children: <Widget>[
            espec.getEspecialistas(),
            ],
      )
    );
  }
}
