import 'package:flutter/material.dart';
import 'package:bhelp/widgets/custom_dailog.dart';
import 'package:bhelp/widgets/game_button.dart';
import 'package:bhelp/main.dart';
import 'package:bhelp/widgets/AppBar.dart' as appBar;
import 'package:bhelp/widgets/Drawer.dart' as drawer;
import 'dart:async';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => new _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<GameButton> buttonsList;
  var player1;
  var activePlayer;

  @override
  void initState() {
    super.initState();
    buttonsList = doInit();
  }

  List<GameButton> doInit() {
    player1 = new List();
    activePlayer = 1;

    var gameButtons = <GameButton>[
      new GameButton(id: 1),
      new GameButton(id: 2),
      new GameButton(id: 3),
      new GameButton(id: 4),
      new GameButton(id: 5),
      new GameButton(id: 6),
      new GameButton(id: 7),
      new GameButton(id: 8),
      new GameButton(id: 9),
      new GameButton(id: 10),
      new GameButton(id: 11),
      new GameButton(id: 12),
    ];
    return gameButtons;
  }

  void playGame(GameButton gb) {
    setState(() {
      Timer timer;
      var seleccionadas=0;
      if (activePlayer == 1) {

        if(gb.id==1) {
          if (seleccionadas == 2) {
            //
          }
          else if (seleccionadas == 1) {
            seleccionadas++;
            player1.add(gb.id);
            if (player1.contains(9)) {
              gb.bg = Colors.red;
              gb.imagen= Image.asset('assets/image/lemon.png');
              gb.enabled = false;
              GameButton(id: 9).enabled = false;
              seleccionadas=0;
            }
            else
              {
                timer = new Timer.periodic(new Duration(milliseconds: 800), (Timer timer) {
                  setState(() {
                    gb.bg = Colors.red;
                    gb.imagen = Image.asset('assets/image/lemon.png');
                  });
                });
                seleccionadas--;
                gb.text = "";
                gb.bg = Colors.grey;
                player1.remove(gb.id);
              }
          }
          else {
            seleccionadas++;
            gb.bg = Colors.red;
            gb.imagen= Image.asset('assets/image/lemon.png');
            player1.add(gb.id);
            gb.enabled=false;
          }
        }
          else if(gb.id==2)
        {
          if (seleccionadas == 2) {
            //
          }
          else if (seleccionadas == 1) {
            seleccionadas++;
            gb.bg = Colors.orange;
            gb.imagen= Image.asset('assets/image/grapes.png');
            player1.add(gb.id);
            if (player1.contains(6)) {
              gb.enabled = false;
              GameButton(id: 6).enabled = false;
              seleccionadas=0;
            }
            else
            {
              seleccionadas--;
              gb.text = "";
              gb.bg = Colors.grey;
              player1.remove(gb.id);
            }
          }
          else {
            seleccionadas++;
            gb.bg = Colors.orange;
            gb.imagen= Image.asset('assets/image/grapes.png');
            player1.add(gb.id);
            gb.enabled=false;
          }
        }
        else if(gb.id==3)
        {
          if (seleccionadas == 2) {
            //
          }
          else if (seleccionadas == 1) {
            seleccionadas++;
            gb.imagen= Image.asset('assets/image/strawberry.png');
            gb.bg = Colors.yellow;
            player1.add(gb.id);
            if (player1.contains(8)) {
              gb.enabled = false;
              GameButton(id: 8).enabled = false;
              seleccionadas=0;
            }
            else
            {
              seleccionadas--;
              gb.text = "";
              gb.bg = Colors.grey;
              player1.remove(gb.id);
            }
          }
          else {
            seleccionadas++;
            gb.imagen= Image.asset('assets/image/strawberry.png');
            gb.bg = Colors.yellow;
            player1.add(gb.id);
            gb.enabled=false;
          }

        }
        else if(gb.id==4)
        {
          if (seleccionadas == 2) {
            //
          }
          else if (seleccionadas == 1) {
            seleccionadas++;
            gb.imagen= Image.asset('assets/image/pineapple.png');
            gb.bg = Colors.greenAccent;
            player1.add(gb.id);
            if (player1.contains(5)) {
              gb.enabled = false;
              GameButton(id: 5).enabled = false;
              seleccionadas=0;
            }
            else
            {
              seleccionadas--;
              gb.text = "";
              gb.bg = Colors.grey;
              player1.remove(gb.id);
            }
          }
          else {
            seleccionadas++;
            gb.imagen= Image.asset('assets/image/pineapple.png');
            gb.bg = Colors.greenAccent;
            player1.add(gb.id);
            gb.enabled=false;
          }

        }
        else if(gb.id==5)
        {
          if (seleccionadas == 2) {
            //
          }
          else if (seleccionadas == 1) {
            seleccionadas++;
            gb.bg = Colors.greenAccent;
            player1.add(gb.id);
            if (player1.contains(4)) {
              gb.enabled = false;
              GameButton(id: 4).enabled = false;
              seleccionadas=0;
            }
            else
            {
              seleccionadas--;
              gb.text = "";
              gb.bg = Colors.grey;
              player1.remove(gb.id);
            }
          }
          else {
            seleccionadas++;
            gb.bg = Colors.greenAccent;
            player1.add(gb.id);
            gb.enabled=false;
          }

        }
        else if(gb.id==6)
        {
          if (seleccionadas == 2) {
            //
          }
          else if (seleccionadas == 1) {
            seleccionadas++;
            gb.bg = Colors.orange;
            gb.imagen= Image.asset('assets/image/grapes.png');
            player1.add(gb.id);
            if (player1.contains(2)) {
              gb.enabled = false;
              GameButton(id: 2).enabled = false;
              seleccionadas=0;
            }
            else
            {
              seleccionadas--;
              gb.text = "";
              gb.bg = Colors.grey;
              player1.remove(gb.id);
            }
          }
          else {
            seleccionadas++;
            gb.bg = Colors.orange;
            gb.imagen= Image.asset('assets/image/grapes.png');
            player1.add(gb.id);
            gb.enabled=false;
          }

        }
        else if(gb.id==7)
        {
          if (seleccionadas == 2) {
            //
          }
          else if (seleccionadas == 1) {
            seleccionadas++;
            gb.imagen= Image.asset('assets/image/raspberry.png');
            gb.bg = Colors.deepPurpleAccent;
            player1.add(gb.id);
            if (player1.contains(12)) {
              gb.enabled = false;
              GameButton(id: 12).enabled = false;
              seleccionadas=0;
            }
            else
            {
              seleccionadas--;
              gb.text = "";
              gb.bg = Colors.grey;
              player1.remove(gb.id);
            }
          }
          else {
            seleccionadas++;
            gb.imagen= Image.asset('assets/image/raspberry.png');
            gb.bg = Colors.deepPurpleAccent;
            player1.add(gb.id);
            gb.enabled=false;
          }

        }
        else if(gb.id==12)
        {
          if (seleccionadas == 2) {
            //
          }
          else if (seleccionadas == 1) {
            seleccionadas++;
            gb.imagen= Image.asset('assets/image/raspberry.png');
            gb.bg = Colors.deepPurpleAccent;
            player1.add(gb.id);
            if (player1.contains(7)) {
              gb.enabled = false;
              GameButton(id: 7).enabled = false;
              seleccionadas=0;
            }
            else
            {
              seleccionadas--;
              gb.text = "";
              gb.bg = Colors.grey;
              player1.remove(gb.id);
            }
          }
          else {
            seleccionadas++;
            gb.imagen= Image.asset('assets/image/raspberry.png');
            gb.bg = Colors.deepPurpleAccent;
            player1.add(gb.id);
            gb.enabled=false;
          }

        }
        else if(gb.id==11)
        {
          if (seleccionadas == 2) {
            //
          }
          else if (seleccionadas == 1) {
            seleccionadas++;
            gb.imagen= Image.asset('assets/image/avocado.png');
            gb.bg = Colors.blue;
            player1.add(gb.id);
            if (player1.contains(10)) {
              gb.enabled = false;
              GameButton(id: 10).enabled = false;
              seleccionadas=0;
            }
            else
            {
              seleccionadas--;
              gb.text = "";
              gb.bg = Colors.grey;
              player1.remove(gb.id);
            }
          }
          else {
            seleccionadas++;
            gb.imagen= Image.asset('assets/image/avocado.png');
            gb.bg = Colors.blue;
            player1.add(gb.id);
            gb.enabled=false;
          }

        }
        else if(gb.id==10)
        {
          if (seleccionadas == 2) {
            //
          }
          else if (seleccionadas == 1) {
            seleccionadas++;
            gb.imagen= Image.asset('assets/image/avocado.png');
            gb.bg = Colors.blue;
            player1.add(gb.id);
            if (player1.contains(11)) {
              gb.enabled = false;
              GameButton(id: 11).enabled = false;
              seleccionadas=0;
            }
            else
            {
              seleccionadas--;
              gb.text = "";
              gb.bg = Colors.grey;
              player1.remove(gb.id);
            }
          }
          else {
            seleccionadas++;
            gb.imagen= Image.asset('assets/image/avocado.png');
            gb.bg = Colors.blue;
            player1.add(gb.id);
            gb.enabled=false;
          }

        }
        else if(gb.id==8)
        {
          if (seleccionadas == 2) {
            //
          }
          else if (seleccionadas == 1) {
            seleccionadas++;
            gb.bg = Colors.yellow;
            gb.imagen= Image.asset('assets/image/strawberry.png');
            player1.add(gb.id);
            if (player1.contains(3)) {
              gb.enabled = false;
              GameButton(id: 3).enabled = false;
              seleccionadas=0;
            }
            else
            {
              seleccionadas--;
              gb.text = "";
              gb.bg = Colors.grey;
              player1.remove(gb.id);
            }
          }
          else {
            seleccionadas++;
            gb.bg = Colors.yellow;
            gb.imagen= Image.asset('assets/image/strawberry.png');
            player1.add(gb.id);
            gb.enabled=false;
          }

        }
        else
        {
          if (seleccionadas == 2) {
            //
          }
          else if (seleccionadas == 1) {
            seleccionadas++;
            gb.bg = Colors.red;
            gb.imagen= Image.asset('assets/image/lemon.png');
            player1.add(gb.id);
            if (player1.contains(1)) {
              gb.enabled = false;
              GameButton(id: 1).enabled = false;
            }
            else
            {
              seleccionadas--;
              gb.text = "";
              gb.bg = Colors.grey;
              player1.remove(gb.id);
            }
          }
          else {
            seleccionadas++;
            gb.bg = Colors.red;
            player1.add(gb.id);
            gb.imagen= Image.asset('assets/image/lemon.png');
            gb.enabled=false;
          }
        }

      }
      int winner = checkWinner();
      if (winner == 1) {
        if (buttonsList.every((p) => p.text != "")) {
          showDialog(
              context: context,
              builder: (_) => new CustomDialog("Juego completado",
                  "Presione el botón para empezar de nuevo.", resetGame));
        }
      }
    });
  }

  int checkWinner() {
    var winner = -1;

    if (player1.contains(1) && player1.contains(2) && player1.contains(3) && player1.contains(4) && player1.contains(5) && player1.contains(6)
    && player1.contains(7) && player1.contains(8) && player1.contains(9)&& player1.contains(11)&& player1.contains(10)&& player1.contains(12)) {
      winner = 1;
    }

    if (winner != -1) {
      if (winner == 1) {
        showDialog(
            context: context,
            builder: (_) => new CustomDialog("Completaste el juego",
                "Presiona el botón si quieres iniciar de nuevo.", resetGame));

      }
    }

    return winner;
  }

  void resetGame() {
    setState(() {
      buttonsList = doInit();
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: appBar.barraSuperior('Memoria'),
        drawer: drawer.menu(context),
        body: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            new Expanded(
              child: new GridView.builder(
                padding: const EdgeInsets.all(10.0),
                gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3,
                    childAspectRatio: 1.0,
                    crossAxisSpacing: 9.0,
                    mainAxisSpacing: 9.0),
                itemCount: buttonsList.length,
                itemBuilder: (context, i) => new SizedBox(
                      width: 100.0,
                      height: 100.0,
                      child: new RaisedButton(
                        padding: const EdgeInsets.all(8.0),
                        onPressed: buttonsList[i].enabled
                            ? () => playGame(buttonsList[i])
                            : null,
                        child: new Text(
                          buttonsList[i].text,
                          style: new TextStyle(
                              color: Colors.white, fontSize: 20.0),
                        ),
                        color: buttonsList[i].bg,
                        disabledColor: buttonsList[i].bg,
                      ),
                    ),
              ),
            ),
            new RaisedButton(
              child: new Text(
                "Reiniciar",
                style: new TextStyle(color: Colors.white, fontSize: 20.0),
              ),
              color: menta,
              padding: const EdgeInsets.all(20.0),
              onPressed: resetGame,
            )
          ],
        ));
  }
}
