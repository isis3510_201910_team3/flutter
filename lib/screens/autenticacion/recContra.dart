import 'package:flutter/material.dart';
import 'package:bhelp/main.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:bhelp/widgets/AlertDialog.dart';
import 'package:bhelp/model/conectivity.dart';



class Recuperar extends StatefulWidget {

  @override
  _Correo createState() => new _Correo();
}

class _Correo extends State<Recuperar> {
  final GlobalKey<FormState> _llave = GlobalKey<FormState>();
  String email = "";
  var deviceSize = null;

  @override
  Widget build(BuildContext context) {
    deviceSize = MediaQuery
        .of(context)
        .size;
    return Scaffold(
        body: Form(
            key: _llave,
            child: ListView(
                children: <Widget>[
                  Container(
                    height: deviceSize.height / 2.5,
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Image.asset(
                            'assets/image/osito.png',
                            width: 90,
                            height: 100,
                          ),
                          Text('Recuperar contraseña',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 26)
                          )
                        ]
                    ),
                    decoration: BoxDecoration(
                        color: rosa,
                        borderRadius: BorderRadius.vertical(
                          bottom: Radius.elliptical(
                              deviceSize.width / 2, deviceSize.height / 6),
                        )
                    ),
                  ),
                  SizedBox(height: deviceSize.height / 8),
                  Padding(
                      padding: EdgeInsets.all(18.0),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                                'Para recuperar tu contraseña ingresa tu correo, da click en el boton "Recuperar" y verifica tu bandeja de entrada.'),
                            SizedBox(height: 8.0),
                            TextFormField(
                              validator: (input) {
                                if (input.isEmpty) {
                                  return 'Diligenciar correo';
                                }
                                if (!input.contains('@') ||
                                    input.contains(' ')) {
                                  return 'Correo inválido';
                                }
                              },
                                onSaved: (input){
                                  email=input;
                                },
                              decoration: InputDecoration(
                                icon: Icon(Icons.mail, color: menta),
                                labelText: 'Correo registrado',
                              ),
                            ),
                            SizedBox(height: 20.0),
                            Material(
                              borderRadius: BorderRadius.circular(20.0),
                              shadowColor: menta,
                              elevation: 5.0,
                              color: menta,
                              child: MaterialButton(
                                minWidth: 1000,
                                height: 42.0,
                                onPressed: () {
                                  recordarContra(context);
                                },
                                child: Text('Recuperar', style: TextStyle(
                                    color: Colors.white,
                                    fontSize: tamanioTexto)),
                              ),
                            ),
                            SizedBox(height: 20.0),
                            Material(
                              borderRadius: BorderRadius.circular(20.0),
                              elevation: 5.0,
                              child: MaterialButton(
                                minWidth: 1000,
                                height: 42.0,
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                child: Text('Volver', style: TextStyle(
                                  color: menta, fontSize: tamanioTexto,
                                )),
                              ),
                            ),

                          ]
                      )
                  )

                ]
            )
        )
    );
  }


  Future<void> recordarContra(context) async {
    bool respuesta = await coneccionState();
    if (!respuesta) {
      showAlertaError(
          context,'Error de autenticación', "No tienes conexión en este momento, por favor recupérala");
    } else {
      print("entra al else");
      final formState = _llave.currentState;
      if (formState.validate()) {
        formState.save();
        try {
          await FirebaseAuth.instance.sendPasswordResetEmail(email: email);
          Navigator.pop(context);
        } catch (e) {
          showAlertaError(context,'Error en el correo',"El correo es inválido, o no esta registrado en la aplicación. Por favor verifique el correo");
          print(e.message);
        }
      }
    }
  }
}