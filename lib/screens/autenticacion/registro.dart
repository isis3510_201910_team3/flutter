import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:bhelp/main.dart';
import 'package:intl/intl.dart';
import 'package:date_format/date_format.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:bhelp/screens/hijos/presentacion_hijos.dart';
import 'package:bhelp/widgets/AlertDialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:bhelp/DAOS/Tutor.dart';
import 'package:bhelp/model/fireStore.dart';

class Registro extends StatefulWidget {
  SharedPreferences sharedPreferences;
  Registro(this.sharedPreferences);
  @override
  _FormularioRegistro createState() => new _FormularioRegistro(sharedPreferences);
}

class _FormularioRegistro extends State<Registro> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  String nombre, email, password, password2,fecha, eps;
  int sexo=0;
  int edad=14;
  int numHijos=0;
  int estrato=-1;
  SharedPreferences sharedPreferences;
  _FormularioRegistro(this.sharedPreferences);

  @override
  Widget build(BuildContext context) {
    var deviceSize =MediaQuery.of(context).size;
    return new Scaffold(
      body:  Form(
              key: _formKey,
              child: new ListView(
                children: <Widget>[
                  Container(
                    height: deviceSize.height/2.5,
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Image.asset(
                            'assets/image/osito.png',
                            width: 90,
                            height: 100,
                          ),
                          Text('Registro',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 24)
                          )
                        ]
                    ),
                    decoration: BoxDecoration(
                        color: rosa,
                        borderRadius: BorderRadius.vertical(
                          bottom: Radius.elliptical(deviceSize.width/2, deviceSize.height/6),
                        )
                    ),
                  ),
                  Padding(
                      padding: EdgeInsets.all(18.0),
                      child:Column(
                          children: <Widget>[
                            TextFormField(
                              inputFormatters: [
                                LengthLimitingTextInputFormatter(35),
                              ],
                              validator: (input){
                                if(input.isEmpty){
                                  return'Ingrese Nombre';
                                }
                                },
                              onSaved: (input){
                                nombre=input;
                                },
                              decoration: InputDecoration(
                                icon: Icon(Icons.person, color: menta),
                                labelText: 'Nombre completo',
                              ),
                            ),
                            TextFormField(
                              inputFormatters: [
                                LengthLimitingTextInputFormatter(35),
                              ],
                              validator: (input){
                                if(input.isEmpty){
                                  return'Ingrese correo';
                                }
                                },
                              onSaved: (input){
                                email=input;
                                },
                              decoration: InputDecoration(
                                icon: Icon(Icons.mail, color: menta),
                                labelText: 'Correo',
                              ),
                            ),
                            TextFormField(
                              obscureText: true,
                              inputFormatters: [
                                LengthLimitingTextInputFormatter(35),
                              ],
                    validator: (input){
                      if(input.isEmpty){
                        return'Ingrese contraseña';
                      }
                      else if(input.length<6){
                        return 'La contraseña debe tener mínimo 6 caracteres';
                      }
                    },
                    onSaved: (input){
                      password=input;
                    },
                    decoration: InputDecoration(
                      icon: Icon(Icons.lock, color: menta),
                      labelText: 'Contraseña',
                    ),
                  ),
                  TextFormField(
                    obscureText: true,
                    inputFormatters: [
                      LengthLimitingTextInputFormatter(35),
                    ],
                    validator: (input){
                      if(input.isEmpty){
                        return'Ingrese contraseña de verificación';
                      }
                      else if(password!=null && password != password2){
                        return 'La contraseña no coincide';
                      }
                    },
                    onSaved: (input){
                      password2=input;
                    },
                    decoration: InputDecoration(
                      icon: Icon(Icons.lock, color: menta),
                      labelText: 'Contraseña (de nuevo)',
                    ),
                  ),
                  DateTimePickerFormField(
                    decoration:InputDecoration(
                      icon:Icon(Icons.cake, color: menta),
                      labelText: 'Fecha de nacimiento',
                    ),
                    inputType: InputType.date,
                    format: DateFormat("d/MM/y"),
                    validator: (input){
                      var fActual=DateTime.now();
                      if(input== null){
                        return'Ingrese fecha';
                      }
                      else if(input.isAfter(fActual)){
                        return 'La fecha ingresada no ha ocurrido';
                      }
                      else if(fActual.difference(input).inDays<5110){
                        return 'Tienes que ser mayor de 14 años para registrarte';
                      }
                    },
                    onSaved: (value) {
                      if(value != null){
                        edad =  (DateTime.now().difference(value).inDays/365).floor();
                        fecha = formatDate(
                          value, [dd, '-', mm, '-', yyyy, ]);
                          }
                          },
                    editable: true,
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                      icon:Icon(Icons.child_care, color: menta),
                      labelText: '¿Cuantos hijos tienes?',
                    ),
                    validator: (input){
                      if(input.isEmpty){
                        return'Ingrese numero de hijos';
                      }
                    },
                    onSaved: (input){
                      if(input.isNotEmpty){
                        numHijos=int.parse(input);
                      }
                    },
                    keyboardType: TextInputType.number,
                    inputFormatters: [
                      LengthLimitingTextInputFormatter(2),
                      WhitelistingTextInputFormatter.digitsOnly,
                    ],
                  ),
                  SizedBox(height: 8.0),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(Icons.account_circle, color: menta),
                        Text('     Género',
                          style: TextStyle(fontSize: tamanioTexto,color: gris),
                        ),
                      ]
                  ),
                  Column(
                      children: <Widget>[
                        Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Radio(
                                value: 1,
                                groupValue: sexo,
                                onChanged: _handleRadioValueChange,
                                activeColor: menta,
                              ),
                              Text('Femenino',
                                style: TextStyle(fontSize: tamanioTexto, color: gris),
                              ),
                            ]

                        ),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Radio(
                                value: 0,
                                groupValue: sexo,
                                onChanged: _handleRadioValueChange,
                                activeColor: menta,
                              ),
                              Text('Masculino',
                                style: TextStyle(fontSize: tamanioTexto, color: gris),),
                            ]
                        ),
                      ]
                  ),
                  TextFormField(
                    inputFormatters: [
                      LengthLimitingTextInputFormatter(35),
                    ],
                    onSaved: (input){
                      if(input.isNotEmpty){
                        eps=input;
                      }
                    },
                    decoration: InputDecoration(
                      icon: Icon(Icons.local_hospital, color: menta),
                      labelText: 'EPS (Opcional)',
                    ),
                  ),
                  TextFormField(
                    keyboardType: TextInputType.number,
                    inputFormatters: [
                      LengthLimitingTextInputFormatter(2),
                      WhitelistingTextInputFormatter.digitsOnly,
                    ],
                    onSaved: (input){
                      if(input.isNotEmpty){
                        estrato=int.parse(input);
                      }
                    },
                    decoration: InputDecoration(
                      icon: Icon(Icons.home, color: menta),
                      labelText: 'Estrato (Opcional)',
                    ),
                  ),
                  SizedBox(height: 16.0),
                  Material(
                    borderRadius: BorderRadius.circular(20.0),
                    shadowColor: menta,
                    elevation: 5.0,
                    color: menta,
                    child: MaterialButton(
                      minWidth: 1000,
                      height: 42.0,
                      onPressed:() {registrar(context);},
                      child: Text('Registrar', style: TextStyle(color: Colors.white, fontSize: tamanioTexto)),
                    ),
                  ),
                ]
            )
          ),
                ],
      )
      ),
    );
  }

  void _handleRadioValueChange(int value) {
    setState(() {
      sexo = value;
    }
    );
  }

  Future<void> registrar(context)async{
    final formState = _formKey.currentState;
    formState.save();
    if(formState.validate()){
      try{
        await FirebaseAuth.instance.createUserWithEmailAndPassword(email: email, password: password2);
        var tutor = new Tutor(nombre,email,fecha,edad,eps,sexo,numHijos,estrato);
        crearTutor(tutor);
        sharedPreferences.setString('correo', email);
        sharedPreferences.setInt('hijos', 0);
        Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder:(context)=>HijosVistaPage()), (Route<dynamic> route) => false);
      }catch(e){
        showAlertaError(context,'Error de autenticación',"Correo inválido o previamente registrado");
        print(e.message);
      }
    }
  }
  Future<void> recordarContra(context)async {
    try{await FirebaseAuth.instance.sendPasswordResetEmail(email: null);
    }catch(e){

    }
  }
}