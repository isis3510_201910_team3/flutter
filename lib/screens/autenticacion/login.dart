import 'package:bhelp/main.dart';
import 'package:bhelp/DAOS/Hijo.dart';
import 'package:bhelp/DAOS/Tareas.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:bhelp/model/fireStore.dart';
import 'package:bhelp/widgets/AlertDialog.dart';
import 'package:bhelp/model/database_helper.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:bhelp/screens/agenda/agenda.dart';
import 'package:bhelp/model/conectivity.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:bhelp/screens/autenticacion/registro.dart';
import 'package:bhelp/screens/autenticacion/recContra.dart';

class Loging extends StatefulWidget {
  SharedPreferences sharedPreferences;
  Loging(this.sharedPreferences);
  @override
  _Form createState() => new _Form(sharedPreferences);
}

class _Form extends State<Loging> {
  final GlobalKey<FormState> _llave = GlobalKey<FormState>();
  String email, password;
  SharedPreferences sharedPreferences;
  var deviceSize =null;
  bool errorLOG = true;
  _Form(this.sharedPreferences);

  @override
  Widget build(BuildContext context) {
    deviceSize =MediaQuery.of(context).size;
    print("Contruye el widget");
    print(deviceSize.height);
    print(deviceSize.width);
    return Scaffold(
      body:Form(
          key: _llave,
          child: ListView(
              children: <Widget>[
              Container(
                height: deviceSize.height/2.5,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Image.asset(
                        'assets/image/osito.png',
                        width: 90,
                        height: 100,
                    ),
                      Text('BHelp',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 26)
                      )
                    ]
                ),
                decoration: BoxDecoration(
                    color: rosa,
                    borderRadius: BorderRadius.vertical(
                      bottom: Radius.elliptical(deviceSize.width/2, deviceSize.height/6),
                    )
                ),
              ),
              SizedBox(height: deviceSize.height/5.5),
              Padding(
                padding: EdgeInsets.all(18.0),
                child:Column(
                  children: <Widget>[
                  SizedBox(height: 8.0),
                  TextFormField(
                    inputFormatters: [
                      LengthLimitingTextInputFormatter(35),
                    ],
                    validator: (input){
                      if(input.isEmpty){
                        return'Diligenciar correo';
                      }
                      if(!input.contains('@')||input.contains(' ')){
                        return'Correo inválido';
                      }
                      },
                    onSaved: (input){
                      email=input;
                      },
                    decoration: InputDecoration(
                      labelText: 'Correo',
                      contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 10.0),
                      border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
                    ),
                  ),
                  SizedBox(height: 8.0),
                  TextFormField(
                    obscureText: true,
                    validator: (input){
                      if(input.isEmpty){
                        return'Diligenciar contraseña';
                      }
                      },
                    onSaved: (input){
                      password=input;
                      },
                    decoration: InputDecoration(
                      labelText: 'Contraseña',
                      contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 10.0),
                      border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
                    ),

                  ),
                  SizedBox(height: 8.0),
                  Material(
                    borderRadius: BorderRadius.circular(20.0),
                    shadowColor: menta,
                    elevation: 5.0,
                    color: menta,
                    child: MaterialButton(
                      minWidth: deviceSize.width,
                      height: 42.0,
                      onPressed:() {inicio(context);},
                      child: Text('Entrar', style: TextStyle(color: Colors.white, fontSize: tamanioTexto)),
                    ),
                  ),
                 // SizedBox(height: 20.0),
                  Row(
                    children: <Widget>[
                      MaterialButton(
                          height: 42.0,
                        onPressed:() {Navigator.push(context, MaterialPageRoute(builder: (context) => Recuperar()));},
                          child: Text('Olvidaste tu contraseña', style: TextStyle(color: gris,
                              decoration: TextDecoration.underline,
                              decorationColor:gris)),
                        ),
                      MaterialButton(
                        height: 42.0,
                        onPressed:() {Navigator.push(context, MaterialPageRoute(builder: (context) => Registro(sharedPreferences)));},
                        child: Text('Registrar ahora', style: TextStyle(color: gris,
                            decoration: TextDecoration.underline,
                            decorationColor:gris)),
                      ),
                    ],
                  )
                  ],
                ),
              )
              ]
          ),
      ));
  }

  Future<void> inicio(context)async{
    bool respuesta = await coneccionState();
    if (!respuesta){
      showAlertaError(context,'Error de autenticación',"No tienes conexión en este momento, por favor recupérala");
    }
    else{
      final formState = _llave.currentState;
      if(formState.validate()){
        formState.save();
        try{
          await FirebaseAuth.instance.signInWithEmailAndPassword(email: email, password: password);
          sharedPreferences.setString('correo', email);
          List<Hijo> lista =await getHijos();
          List<Tareas> tarea1 =await getTareas("Alimentos");
          List<Tareas> tarea2 =await getTareas("Medicinas");
          List<Tareas> tarea3 =await getTareas("Citas");
          if(lista.isNotEmpty){
            sharedPreferences.setInt('hijos', lista.length);
            var db = new DatabaseHelper();
            db.sincronizar(lista,tarea1,tarea2,tarea3);
          }
          else
            sharedPreferences.setInt('hijos', 0);
          Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => AgendaPage()), (Route<dynamic> route) => false);
        } catch(e){
          showAlertaError(context,'Error de autenticación',"Correo o contraseña incorrecto");
          print(e.message);
        }
      }
    }
  }

}