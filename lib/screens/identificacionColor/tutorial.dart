import 'package:flutter/material.dart';
import 'package:bhelp/main.dart';
// Widgets utilizados
import 'package:bhelp/widgets/AppBar.dart' as appBar;
import 'package:bhelp/widgets/Drawer.dart' as drawer;
import 'package:bhelp/screens/identificacionColor/cambioColores.dart' as colores;


class TutorialPage extends StatefulWidget {
  TutorialPage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _TutorialPageState createState() => _TutorialPageState();
}

class _TutorialPageState extends State<TutorialPage> {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: appBar.barraSuperior("Sensibilidad a colores"),
        drawer: drawer.menu(context),
        body: Padding(
          padding: EdgeInsets.all(30.0),
          child: SingleChildScrollView(
            child: new Center(
              child:
              Column( children: <Widget>[
                SizedBox(height: 8.0),
                new Image.asset(
                  'assets/image/niño.png' ,width: 70,height: 70,
                ),
                Text(' ', style: TextStyle(fontSize: 70)),
                Text(
                  'El siguiente test es realizado para reconocer si su hijo es capaz de detectar los cambios de tonalidad. Si su hijo mantiene la mirada en los colores y reacciona a su cambio, sabemos que puede identificarlos. '
                    , style: TextStyle(fontSize: 20), textAlign: TextAlign.justify,
                ),
                Text(' ', style: TextStyle(fontSize: 70)),

                Material(
                  borderRadius: BorderRadius.circular(20.0),
                  shadowColor: menta,
                  elevation: 5.0,
                  color: menta,
                  child: MaterialButton(
                    minWidth: 1000,
                    height: 42.0,
                    onPressed:() {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => colores.CambioColoresSensibilidad()));
                    },
                    child: Text('Continuar', style: TextStyle(color: Colors.white, fontSize: tamanioTexto)),
                  ),
                ),


              ],

              ),
            ),
          ),

        )
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

}