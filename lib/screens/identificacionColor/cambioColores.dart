import 'package:flutter/material.dart';
import 'dart:math';
import 'dart:async';
import 'package:bhelp/widgets/AppBar.dart' as appBar;
import 'package:bhelp/widgets/Drawer.dart' as drawer;

class CambioColoresSensibilidad extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'BHelp_colores',
      theme: ThemeData(
          primaryColor: Color.fromARGB(255, 254, 90, 90),
          fontFamily: 'Roboto'
      ),
      home: CambioColores(title: 'Sensibilidad a colores '),
    );
  }
}
class CambioColores extends StatefulWidget {
  CambioColores({Key key, this.title}) : super(key: key);

  final String title;

  _CambioColoresState createState() => _CambioColoresState();
}

class _CambioColoresState extends State<CambioColores> with TickerProviderStateMixin{

  List colors = [Colors.green, Colors.blueAccent, Colors.blue, Colors.greenAccent, Colors.redAccent, Colors.red, Colors.lightGreen, Colors.teal,
  Colors.black, Colors.tealAccent, Colors.blueGrey, Colors.grey, Colors.white, Colors.lightGreenAccent, Colors.orangeAccent, Colors.orange, Colors.deepOrangeAccent,
  Colors.deepOrange, Colors.amber, Colors.amberAccent, Colors.brown, Colors.black12, Colors.cyan, Colors.cyanAccent, Colors.deepPurple, Colors.deepPurpleAccent,
  Colors.indigo, Colors.indigoAccent, Colors.lightBlue,Colors.lightBlueAccent,
  Colors.lime, Colors.limeAccent, Colors.pink, Colors.pinkAccent, Colors.yellow, Colors.purple, Colors.purpleAccent, Colors.teal, Colors.tealAccent];

  Random random = new Random();
  Timer timer;

  int a = 0;
  int b = 0;
  int c = 0;
  int d = 0;
  int e = 0;
  int f = 0;

  @override
  void initState() {
    super.initState();

    timer = new Timer.periodic(new Duration(milliseconds: 800), (Timer timer) {
      setState(() {
        changeIndexA();
      });
    });
  }

  void changeIndexA() {
    setState(
            () => a = random.nextInt(colors.length-5)
    );
  }
  @override
  void dispose() {
    super.dispose();
    timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar.barraSuperior(widget.title),
      drawer: drawer.menu(context),
      body: CustomScrollView(
        slivers: <Widget>[
          SliverGrid.count(
            mainAxisSpacing: 1,
            crossAxisSpacing: 1,
            crossAxisCount: 2,
            children: [
              Container(color: colors[a], height: 250.0),
              Container(
                  color: (colors[a+1]!=null)? (colors[a+1]):(colors[a-1]),
                  height: 250.0
              ),
              Container(
                  color: (colors[a+2]!=null)? (colors[a+2]):(colors[a-2]),
                  height: 250.0
              ),
              Container(
                  color: (colors[a+3]!=null)? (colors[a+3]):(colors[a-3]),
                  height: 250.0
              ),
              Container(
                  color: (colors[a+4]!=null)? (colors[a+4]):(colors[a-4]),
                  height: 250.0
              ),
              Container(
                  color: (colors[a+5]!=null)? (colors[a+5]):(colors[a-5]),
                  height: 250.0
              ),

            ],
          ),
        ],
      ),
    );
  }

}

