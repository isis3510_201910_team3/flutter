import 'package:flutter/material.dart';
import 'package:bhelp/widgets/AppBar.dart';
import 'package:bhelp/model/database_helper.dart';
import 'package:bhelp/screens/hijos/presentacion_hijos.dart';
import 'package:bhelp/widgets/AlertDialog.dart';
import 'package:bhelp/widgets/Drawer.dart';
import 'package:bhelp/model/fireStore.dart';
import 'package:flutter/services.dart';
import 'package:bhelp/main.dart';
import 'package:bhelp/model/conectivity.dart';

class Autorizar extends StatefulWidget {
  List<String> hijos;
  Autorizar(this.hijos);

  @override
  _AutorizarForm createState() => new _AutorizarForm(hijos);
}

class _AutorizarForm extends State<Autorizar> {
  List<String> hijos;
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  String  email;
  String idHijo;
  _AutorizarForm(this.hijos){
    idHijo= hijos[0];
  }



  @override
  Widget build(BuildContext context) {

  // _hijos();
    return new Scaffold(
        appBar: barraSuperior("Autorizar otro usuario"),
        drawer: menu(context),
        body: Form(
          key: _formKey,
          child: new ListView(
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.all(18.0),
                  child:Column(
                      children: <Widget>[
                        SizedBox(height: 100),
                        Text(
                            'Escribe el correo de la persona con la que vas a compartir la información de tu hijo'),
                        SizedBox(height: 20),
                        TextFormField(
                          inputFormatters: [
                            LengthLimitingTextInputFormatter(35),
                          ],
                          validator: (input){
                            if(input.isEmpty){
                              return'Ingrese correo';
                            }
                          },
                          onSaved: (input){
                            email=input;
                          },
                          decoration: InputDecoration(
                            icon: Icon(Icons.mail, color: menta),
                            labelText: 'Correo',
                          ),
                        ),
                        SizedBox(height: 16.0),
                        Container(
                            height: 150,
                            child: opci(context)
                        ),

                        SizedBox(height: 16.0),
                        Material(
                          borderRadius: BorderRadius.circular(20.0),
                          shadowColor: menta,
                          elevation: 5.0,
                          color: menta,
                          child: MaterialButton(
                            minWidth: 1000,
                            height: 42.0,
                            onPressed:() {registrar(context);},
                            child: Text('Autorizar', style: TextStyle(color: Colors.white, fontSize: tamanioTexto)),
                          ),
                        ),
                      ]
                  )
              ),
            ],
          )
      ),
    );
  }
  Widget opci(BuildContext context) {
    return new ListView.builder(
        itemCount: hijos == null ? 0 : hijos.length,
        shrinkWrap: true,
        itemBuilder: (BuildContext context, int index) {
          return Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Radio(
                  value: hijos[index],
                  groupValue: idHijo,
                  onChanged: _handleRadioValueChange,
                  activeColor: menta,
                ),
                Text( text(hijos[index]),
                  style: TextStyle(fontSize: tamanioTexto, color: gris),
                ),
              ]
          );

        });
  }

   _hijos() async {
    int numhijos=sharedPreferencesFull.getInt('hijos');
    if(numhijos>0) {
      var db = new DatabaseHelper();
      hijos = await db.getNombreHijos();
    }
    else{
      showAlerta(context,'No tienes hijos registrados',HijosVistaPage(),"Tienes que registrar un hijo para poder agregar tareas");
    }
  }
  void _handleRadioValueChange(String value) {
    setState(() {
      idHijo = value;
    }
    );
  }
  String text(String opcion){
    List<String> id=opcion.split('-');
    String texto=id[0];
    return texto;
  }

  Future<void> registrar(context)async{
    final formState = _formKey.currentState;

    bool respuesta = await coneccionState();
    if (!respuesta){
      showAlertaError(context,'Error de autenticación',"No tienes conexión en este momento, por favor recupérala");
    }
    else{
      if(formState.validate()){
        formState.save();
        String tutor = await getTutor(email);
        if(tutor == null){
          showAlertaError(context,'Error',"Este correo no se encuentra registrado, verifique la dirección de email");
        }
        else{
          await crearAutorizacion(email,idHijo);
          showAlertaError(context,'Autorización éxitosa',"El usuario "+tutor+" fue autorizado");
        }
    }
    }
  }
}