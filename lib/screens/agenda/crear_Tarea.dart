import 'package:intl/intl.dart';
import 'package:bhelp/main.dart';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:bhelp/DAOS/Tareas.dart';
import 'package:bhelp/model/database_helper.dart';
import 'package:date_format/date_format.dart';
import 'package:bhelp/model/fireStore.dart' as remoto;
import 'package:bhelp/widgets/local_notifications_helper.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class MyHomePage extends StatefulWidget {
  List<String> hijos;
  MyHomePage(this.hijos);
  @override
  MyHomePageState createState() => MyHomePageState(hijos);
}

class MyHomePageState extends State<MyHomePage> {
  List<String> hijos;
  final flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
  var _formKey = GlobalKey<FormState>();

  Tareas _data= new Tareas("","", "", "", "", "", "");
  var fecha;
  var hora;

  List<String> _options = ["Alimentos", "Medicinas", "Citas"];

  List<DropdownMenuItem<String>> _dropDownMenuItems;
  String _currentOption;

  List<String> _options2 = ["Alta", "Media", "Baja"];

  List<DropdownMenuItem<String>> _dropDownMenuItems2;
  String _currentOption2;

  MyHomePageState(this.hijos){
    _data.hijo=hijos[0];
  }

  @override
  void initState() {
    _dropDownMenuItems = getDropDownMenuItems(_options);
    _currentOption = _dropDownMenuItems[0].value;
    _data.categoria= _currentOption;
    super.initState();

    _dropDownMenuItems2 = getDropDownMenuItems(_options2);
    _currentOption2 = _dropDownMenuItems2[0].value;
    _data.prioridad= _currentOption2;
    super.initState();

    final settingsAndroid = AndroidInitializationSettings('bebes');
    final settingsIOS = IOSInitializationSettings(
        onDidReceiveLocalNotification: (id, title, body, payload) =>
        null);

    flutterLocalNotificationsPlugin.initialize(
        InitializationSettings(settingsAndroid, settingsIOS),);
  }

  List<DropdownMenuItem<String>> getDropDownMenuItems(List<String> lista) {
    List<DropdownMenuItem<String>> items = new List();
    for (String option in lista) {
      items.add(new DropdownMenuItem(
          value: option,
          child: new Text(option)
      ));
    }
    return items;
  }

  void changedDropDownItem(String selectedOption) {
    _data.categoria= selectedOption;
    setState(() {
      _currentOption = selectedOption;
    });
  }
  void changedDropDownItem2(String selectedOption2) {
    _data.prioridad=selectedOption2;
    setState(() {
      _currentOption2 = selectedOption2;
    });
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);

    return new Scaffold(
      appBar: new AppBar(
          title: new Text('Nueva tarea'),
          actions: <Widget> [
            new FlatButton(
                child: new Text('GUARDAR',
                    style: theme.textTheme.body1.copyWith(color: Colors.white)),
                onPressed: () { _save(context);}
            )
          ]
      ),
      body: new Form(
        key: _formKey,
        child: new ListView(
          padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 16.0),
          children: <Widget>[
            SizedBox(height: 8.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children:[
                Image.asset(
                  'assets/image/notification.png' ,width: 70,height: 70,
                ),
              ],
            ),
            SizedBox(height: 8.0),
            Row(
                children: <Widget>[
                  Align(
                  alignment: Alignment.centerLeft,
                  child: Row(
                    children:[
                      Icon(Icons.category, color: menta),
                      Text("   Categoría   ", style: TextStyle(
                        color: Colors.black54, fontSize: 17.0
                    )

                    ),
          ],
                  ),
                ),
                Align(
                  alignment: Alignment.center,
                  child: Container(
                      child: new DropdownButton(
                        value:_currentOption,
                        items: _dropDownMenuItems,
                        hint: new Text("Categoría"),
                        onChanged: changedDropDownItem,
                      )
                  ),
                ),
                ]
            ),
            Row(
                children: <Widget>[
                  Align(
                    alignment: Alignment.center,
                    child: Row(
                      children:[
                        Icon(Icons.priority_high, color: menta),
                        Text("   Prioridad   ", style: TextStyle(
                            color: Colors.black54, fontSize: 17.0
                        )

                        ),
                      ],
                    ),
                  ),
                Align(
                  alignment: Alignment.center,
                  child: Container(
                      child: new DropdownButton(
                        value: _currentOption2,
                        items: _dropDownMenuItems2,
                        hint: new Text("Prioridad"),
                        onChanged: changedDropDownItem2,
                      )
                  ),
                ),
                ]
            ),

            DateTimePickerFormField(
              decoration:InputDecoration(
                icon:Icon(Icons.date_range, color: menta),
                labelText: 'Fecha y hora',
              ),
              inputType: InputType.both,
              format: DateFormat("d/MM/y HH:mm aaa"),
              editable: true,

              validator: (value) {
                if (value ==null ) {
                  return ('Ingrese una fecha');
                }
                else if(value.isBefore(DateTime.now()) ){
                  return ('Ingrese una fecha posterior a la actual');
                }
              },
              onSaved: (value) {

                if(value!=null) {
                  fecha= value;
                  _data.fecha = formatDate(
                      value,
                      [dd, '-', mm, '-', yyyy, ' ', hh, ':', nn, ' ', am]
                  );

                }
              },
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                child: new TextFormField(
                  keyboardType: TextInputType.text,
                  inputFormatters: [
                    LengthLimitingTextInputFormatter(25),
                  ],

                  validator: (value) {
                    if (value.length < 2) {
                      return ('El título debe tener al menos 2 caracteres');
                    }
                    else if(value.isEmpty){
                      return ('Ingrese un título para la tarea');
                    }
                  },
                  onSaved: (value) {
                    _data.titulo = value;
                  },
                  decoration: InputDecoration(
                    icon: Icon(Icons.title, color: menta),
                    labelText: 'Título',
                  ),


                ),
              ),
            ),
            new Text("      ", style: TextStyle(
                color: Colors.white, fontSize: 10.0
            )

            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                child: new TextFormField(
                  keyboardType: TextInputType.text,
                  inputFormatters: [
                    LengthLimitingTextInputFormatter(40),
                  ],
                  onSaved: (value) {
                    _data.descripcion = value;
                  },
                  decoration: InputDecoration(
                    icon: Icon(Icons.description, color: menta),
                    labelText: 'Descripción (opcional)',
                  ),
                ),
              ),
            ),
            SizedBox(height: 8.0),
            Text(' '),
            Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Icon(Icons.child_care, color: menta),
                  Expanded(child: Text(' Selecciona al hijo al que pertenece la tarea:',
                    style: TextStyle(fontSize: tamanioTexto,color: gris),
                  ),
                  ),

                ]
            ), Container(
                height: 150,
                child: opci(context)
         ),
          ],
        ),
      ),
    );

  }
  Widget opci(BuildContext context) {
    return new ListView.builder(
        itemCount: hijos == null ? 0 : hijos.length,
        shrinkWrap: true,
        itemBuilder: (BuildContext context, int index) {
          return Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Radio(
                  value: hijos[index],
                  groupValue: _data.hijo,
                  onChanged: _handleRadioValueChange,
                  activeColor: menta,
                ),
                Text( text(hijos[index]),
                  style: TextStyle(fontSize: tamanioTexto, color: gris),
                ),
              ]
          );

        });
  }

  String text(String opcion){
    List<String> id=opcion.split('-');
    String texto=id[0];
    return texto;
  }

  void _handleRadioValueChange(String value) {
    setState(() {
      _data.hijo = value;
    }
    );
  }
  Future addRecord() async {
    var db = new DatabaseHelper();
    var tarea = new Tareas(_data.id,_data.categoria, _data.prioridad, _data.titulo, _data.descripcion, _data.fecha,_data.hijo);
    await db.saveTarea(tarea);
    remoto.crearTareaCategoria(tarea);
    remoto.crearTareaHijo(tarea);
  }

  Future<void> _save(context) async{
    final formState = _formKey.currentState;
    formState.save();
    if(formState.validate()){
      try{
        _data.id=_data.titulo+"-"+_data.fecha;
        addRecord();
        showIconNotification(context, flutterLocalNotificationsPlugin, title: "Tarea agregada para el " + _data.fecha, body: 'Recordatorio programado', icon: Image.asset('assets/image/bebes.png'));
        Navigator.of(context).pop(_data);
      }catch(e){
        Navigator.of(context).pop();
        print(e.message);
      }
    }
  }

}
