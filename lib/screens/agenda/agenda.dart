import 'package:bhelp/main.dart';
import 'package:flutter/material.dart';
import 'crear_Tarea.dart' as Dialogo;
import 'package:bhelp/DAOS/Tareas.dart' as Tarea;
import 'package:bhelp/widgets/Drawer.dart' as drawer;
import 'package:bhelp/widgets/Center.dart' as centro;
import 'package:bhelp/presenters/visualizaPantalla.dart';
import 'package:bhelp/widgets/AlertDialog.dart';
import 'package:bhelp/screens/hijos/presentacion_hijos.dart';
import 'package:bhelp/model/database_helper.dart';

class AgendaPage extends StatefulWidget {
  AgendaPage({Key key, this.title5}) : super(key: key);
  final String title5;

  @override
  _AgendaPageState createState() => _AgendaPageState();
}

class _AgendaPageState extends State<AgendaPage> implements Vista {
  List tareas = [];
  VisualizaPantalla homePresenter;

  @override
  void initState() {
    super.initState();
    homePresenter = new VisualizaPantalla(this);
  }

  displayRecord() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          bottom: TabBar(
            tabs: [
              Tab(icon: Icon(Icons.local_dining), text: "Alimentos",),
              Tab(icon: Icon(Icons.local_hospital), text: "Medicinas"),
              Tab(icon: Icon(Icons.notifications), text: "Citas"),
            ],
          ),
          title: Text('Agenda BHelp'),
        ),
        drawer: drawer.menu(context),
        body: TabBarView(
          children: [
            centro.listaTareas(homePresenter, 'Alimentos', 'assets/image/groceries.png', 'Aún no tienes tareas de alimentos'),
            centro.listaTareas(homePresenter, 'Medicinas', 'assets/image/medicine.png','Aún no tienes tareas de medicinas'),
            centro.listaTareas(homePresenter, 'Citas', 'assets/image/calendar (1).png', 'Aún no tienes tareas de citas'),
          ],
        ),

        floatingActionButton: FloatingActionButton(
          onPressed: () => _openDialogAddItem(),
          tooltip: 'Agenda',
          child: new Icon(Icons.add),
          backgroundColor: menta,
        ), // This trailing comma makes auto-formatting nicer for build methods.
      ),
    );
  }

  @override
  void screenUpdate() {
    setState(() {});
  }

  Future _openDialogAddItem() async {
    int hijos=sharedPreferencesFull.getInt('hijos');
    if(hijos>0){
      var db = new DatabaseHelper();
      List<String> hijos=await db.getNombreHijos();
      Tarea.Tareas data = await Navigator.of(context).push(new MaterialPageRoute<Tarea.Tareas>(
          builder: (BuildContext context) {
            return new Dialogo.MyHomePage(hijos);
          },
          fullscreenDialog: true));
      setState(() {
        tareas.add(data);
      });
    }
    else{
      showAlerta(context,'No tienes hijos registrados',HijosVistaPage(),"Tienes que registrar un hijo para poder agregar tareas");
    }


  }
}
