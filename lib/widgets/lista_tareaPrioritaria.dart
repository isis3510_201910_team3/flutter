import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:bhelp/DAOS/Tareas.dart';
import 'package:bhelp/presenters/visualizaPantalla.dart';
import 'package:bhelp/screens/agenda/edicionTarea.dart' as Editor;
import 'package:bhelp/widgets/DeleteDialog.dart' as dialogo;

var idActual;

class TareasPrioritariasList extends StatelessWidget {
  List<Tareas> item;
  VisualizaPantalla visualizacionPantalla;

  TareasPrioritariasList(List<Tareas> this.item,
      VisualizaPantalla this.visualizacionPantalla, {
        Key key,
      }) : super(key: key);

  @override
  Widget build(BuildContext context,) {
    return new ListView.builder(
      shrinkWrap: true,
        itemCount: item == null ? 0 : item.length,
        itemBuilder: (BuildContext context, int index) {
          idActual = index;
          return new Card(
            child: new Container(
                child: new Center(
                  child: new Row(
                    children: <Widget>[
                      new CircleAvatar(
                        radius: 20.0,
                        backgroundColor: Colors.redAccent,
                        child: new Text("!!!",
                          style: new TextStyle(
                            fontSize: 24.0,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      new Expanded(
                        child: new Padding(
                          padding: EdgeInsets.all(10.0),
                          child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Text(
                                (item[index].titulo),
                                // set some style to text
                                style: new TextStyle(
                                    fontSize: 16.0, color: Colors.black),
                              ),
                              new Text(
                                "Fecha: " + item[index].fecha,
                                // set some style to text
                                style: new TextStyle(
                                    fontSize: 15.0, color: Colors.black54),
                              ),
                            ],
                          ),
                        ),
                      ),

                    ],
                  ),
                ),
                padding: const EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0)
            ),
          );
        });
  }

}