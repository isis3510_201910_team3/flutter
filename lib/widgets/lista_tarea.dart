import 'package:bhelp/main.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:bhelp/DAOS/Tareas.dart';
import 'package:bhelp/presenters/visualizaPantalla.dart';
import 'package:bhelp/screens/agenda/edicionTarea.dart' as Editor;
import 'package:bhelp/widgets/DeleteDialog.dart' as dialogo;
import 'package:bhelp/model/database_helper.dart';
import 'package:bhelp/screens/hijos/presentacion_hijos.dart';
import 'package:bhelp/widgets/AlertDialog.dart';

var idActual;

class TareasList extends StatelessWidget {
  List<Tareas> item;
  VisualizaPantalla visualizacionPantalla;

  TareasList(
      List<Tareas> this.item,
      VisualizaPantalla this.visualizacionPantalla, {
        Key key,
      }) : super(key: key);

  @override
  Widget build(BuildContext context,) {
    return new ListView.builder(
        itemCount: item == null ? 0 : item.length,
        itemBuilder: (BuildContext context, int index) {
          idActual = index;
          return new Card(
            child: new Container(
                child: new GestureDetector(
                  onTap: () => _openDialogEditItem(item[index], context),
                  child: new Center(
                    child: new Row(
                      children: <Widget>[
                        new CircleAvatar(
                          radius: 20.0,
                          backgroundColor: (item[index].prioridad == 'Alta')
                              ? Colors.redAccent
                              : ((item[index].prioridad == 'Media')
                              ? Colors.orangeAccent
                              : Colors.lightGreen),
                          child: new Text(
                            (item[index].prioridad == 'Alta')
                                ? "!!!"
                                : ((item[index].prioridad == 'Media')
                                ? "!!"
                                : "!"),
                            style: new TextStyle(
                              fontSize: 24.0,
                              color: Colors.white,
                            ),
                          ),
                        ),
                        new Expanded(
                          child: new Padding(
                            padding: EdgeInsets.all(10.0),
                            child: new Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                new Text(
                                  (item[index].titulo!=null)? ( item[index].titulo): ("Titulo" ),
                                  // set some style to text
                                  style: new TextStyle(
                                      fontSize: 16.0, color: Colors.black),
                                ),
                                new Text(
                                   (item[index].fecha!=null)? ("Fecha: " + item[index].fecha): ("Fecha: " ),
                                  //"Fecha: " + item[index].fecha,
                                  // set some style to text
                                  style: new TextStyle(
                                      fontSize: 15.0, color: Colors.black54),
                                ),
                              ],
                            ),
                          ),
                        ),
                        new Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            new IconButton(
                                icon: const Icon(Icons.check_box_outline_blank,
                                    color: Colors.blueGrey),
                                onPressed: () {

                                  dialogo.deleteDialog(context, 'assets/image/calendar (1).png', 'Tarea finalizada', 'La tarea actual se marcará como completada y se eliminará de forma permanente. ¿Desea continuar?').then((value) {
                                    if(value) {
                                      visualizacionPantalla.delete(item[index]);
                                    }
                                  });


                                }),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                padding: const EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0)),
          );
        });
  }

  displayRecord() {
    visualizacionPantalla.updateScreen();
  }

  Future _openDialogEditItem(Tareas tarea, BuildContext context) async {
    int hijos=sharedPreferencesFull.getInt('hijos');
    if(hijos>0) {
      var db = new DatabaseHelper();
      List<String> hijos = await db.getNombreHijos();
      Tareas data = await Navigator.of(context).push(
          new MaterialPageRoute<Tareas>(
              builder: (BuildContext context) {
                return new Editor.MyHomePage(
                    tarea, visualizacionPantalla, hijos);
              },
              fullscreenDialog: true));
      visualizacionPantalla.updateScreen();
    }
    else{
      showAlerta(context,'No tienes hijos registrados',HijosVistaPage(),"Tienes que registrar un hijo para poder agregar tareas");
    }


  }
}
