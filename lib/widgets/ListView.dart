//Librerias
import 'package:flutter/material.dart';
import 'package:bhelp/model/conectivity.dart';
import 'package:bhelp/widgets/AlertDialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:bhelp/screens/autorizar.dart';
import 'package:bhelp/model/database_helper.dart';
import 'package:bhelp/main.dart';

// Widgets utilizados
import 'ListView.dart' as Lista;

ListTile opcionMenu(Icon icono, String opcion, BuildContext context, Widget page){
  return new ListTile(
    leading: icono,
    title: Text(opcion),
    onTap: (){
    Navigator.push(context,
      MaterialPageRoute(builder: (context) => page));
    }
  );
}




ListTile opcionMenuAutorizar(Icon icono, String opcion, BuildContext context){
  return new ListTile(
      leading: icono,
      title: Text(opcion),
      onTap: (){
        _autorizacion(context);

      }
  );
}

Future _autorizacion( BuildContext context) async {
  int hijos=sharedPreferencesFull.getInt('hijos');
  if(hijos>0) {
    var db = new DatabaseHelper();
    List<String> hijos=await db.getNombreHijos();
    Navigator.push(context, MaterialPageRoute(builder: (context) =>Autorizar(hijos)));
  }
  else{
   print ("AIYUDAAAA NO CHE QUE HACER");
    //showAlerta(context,'No tienes hijos registrados',HijosVistaPage(),"Tienes que registrar un hijo para poder agregar tareas");
  }

}




ListTile opcionClose(Icon icono, String opcion, BuildContext context, Widget page,SharedPreferences llave){
  return new ListTile(
      leading: icono,
      title: Text(opcion),
      onTap: (){
        showAlertaCerrar( context, '¿Deseas cerrar sesión?', page,llave);
      }
  );
}

ListTile opcionMenuCon(Icon icono, String opcion, BuildContext context, Widget page, String alertaMen){

  return new ListTile(
      leading: icono,
      title: Text(opcion),
      onTap: ()async{
        bool respuesta = await coneccionState();
        if (respuesta){
          Navigator.push(context, MaterialPageRoute(builder: (context) => page));
        }
        else{
          showAlerta(context,'Alerta de conexión',page,alertaMen);
        }
      }
  );
}

