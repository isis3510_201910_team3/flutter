import 'package:flutter/material.dart';

class GameButton {
  final id;
  String text;
  Color bg;
  bool enabled;
  Image imagen= new Image.asset('assets/image/osito.png');

  GameButton(
      {this.id, this.text = "", this.bg = Colors.grey, this.enabled = true, this.imagen});
}
