import 'package:flutter/material.dart';
import 'package:bhelp/widgets/lista_tarea.dart';
import 'package:bhelp/widgets/lista_vacunas_edad.dart';
import 'package:bhelp/widgets/lista_tareaPrioritaria.dart';
import 'package:bhelp/presenters/visualizaPantalla.dart';
import 'package:bhelp/DAOS/Tareas.dart';
import 'package:bhelp/model/conectivity.dart';
import 'package:bhelp/model/fireStore.dart' as fire;
import 'package:bhelp/DAOS/Vacuna.dart';
import 'package:bhelp/main.dart';
import 'package:bhelp/widgets/AlertDialog.dart';
import 'package:flutter/services.dart';


Center listaTareas(VisualizaPantalla pantalla, String tipo, String image, String texto){
  return Center(
    child: new FutureBuilder<List<Tareas>>(
      future: getTareas(pantalla,tipo),
      builder: (context, snapshot) {
        if (snapshot.hasError) print(snapshot.error);
        var data = snapshot.data;
        if(data!=null) {
          return (data.length > 0)
              ? new TareasList(data, pantalla)
              : new Padding(padding: EdgeInsets.fromLTRB(10, 20, 20, 20),
          child: new SingleChildScrollView(
            child: new Column(
              children: <Widget>[
                new Image.asset(
                  image,
                  width: 150,
                  height: 150,
                ),
                new Text('  '),
                new Text(texto, style: TextStyle(fontSize: 15.0),),
                new Text('¡Ingresa una!', style: TextStyle(fontSize: 15.0, color: Colors.redAccent, fontWeight: FontWeight.bold ),),
              ],
            ),
          ),
          
          );
        }
        else
          {
            return new CircularProgressIndicator();
          }
      },
    ),
  );
}

Future<List<Tareas>> getTareas(VisualizaPantalla pantalla,String tipo) async{
  bool respuesta = await coneccionState();
  if(respuesta){
    return fire.getTareas(tipo);
  }
  else{
    return pantalla.getTareas(tipo);
  }
}


Center listaTareasPrioridadAlta(VisualizaPantalla pantalla, String prioridad, String image, String texto, String idHijo){
  return Center(
    child: new FutureBuilder<List<Tareas>>(
      future: getTareasPrioridad(pantalla,prioridad,idHijo),
      builder: (context, snapshot) {
        if (snapshot.hasError) print(snapshot.error);
        var data = snapshot.data;
        if(data!=null) {
          return (data.length > 0)
              ? new TareasPrioritariasList(data, pantalla)
              : new Column(
                children: <Widget>[
                  Text(''),
                  Text(''),
                  new Image.asset(
                    image,
                    width: 50,
                    height: 50,
                  ),
                  new Text('  '),
                  new Text(texto, style: TextStyle(fontSize: 15.0),),
                ],

          );
        }
        else
        {
          return new Column(
            children: <Widget>[
              Text(''),
              Text(''),
              new Image.asset(
                'assets/image/stopwatch.png',
                width: 50,
                height: 50,
              ),
            ],

          );
        }
      },
    ),
  );
}

Future<List<Tareas>> getTareasPrioridad(VisualizaPantalla pantalla,String prioridad,String idHijo) async{

  bool respuesta = await coneccionState();
  if(respuesta){
    return fire.getTareasPrioridad(prioridad, idHijo);
  }
  else {
    return pantalla.getTareasPrioridad(prioridad);
  }
}


Center listaVacunasEdad(VisualizaPantalla pantalla, String edad, void accion){
  return Center(
    child: new FutureBuilder<List<Vacuna>>(
      future: getVacunasEdad(pantalla, edad),
      builder: (context, snapshot) {
        if (snapshot.hasError) print(snapshot.error);
        var data = snapshot.data;
        if(data!=null) {
          return VacunasList(data, pantalla, accion);
        }
        else
        {
          return new Column(
            children: <Widget>[
              Text(''),
              Text(''),
              new Image.asset(
                'assets/image/stopwatch.png',
                width: 50,
                height: 50,
              ),
            ],

          );
        }
      },
    ),
  );
}

Future<List<Vacuna>> getVacunasEdad(VisualizaPantalla pantalla,String edad) async{

  bool respuesta = await coneccionState();
  if(respuesta){
    return fire.getVacunasEdad(edad);
  }
  else {
    return List<Vacuna>();
  }
}
