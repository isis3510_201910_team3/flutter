import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:bhelp/model/database_helper.dart';
import 'package:firebase_auth/firebase_auth.dart';


void showAlerta(BuildContext context,String titulo, Widget page, String mensaje){
  showDialog(
      context: context,
      builder: (BuildContext context){
        return AlertDialog(
          title: Text(titulo) ,
          content: Text(mensaje),
          actions: <Widget>[new FlatButton(
            child: new Text('Aceptar'),
            onPressed: (){
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => page));
            },
          )
          ],

        );
      }
  );
}
void showAlertaError(BuildContext context,String titulo, String mensaje){
  showDialog(
      context: context,
      builder: (BuildContext context){
        return AlertDialog(
          title: Text(titulo) ,
          content: Text(mensaje),
          actions: <Widget>[new FlatButton(
            child: new Text('Aceptar'),
            onPressed: (){
              Navigator.pop(context);
            },
          )
          ],

        );
      }
  );
}

void showAlertaCerrar(BuildContext context, String mensaje, Widget page,SharedPreferences llave){
  showDialog(
      context: context,
      builder: (BuildContext context){
        return AlertDialog(
          title: Text('Cerrar sesión') ,
          content: Text(mensaje),
          actions: <Widget>[FlatButton(
            child: new Text('Cancelar'),
            onPressed: (){
              Navigator.pop(context);
            },
          ),FlatButton(
            child: new Text('Aceptar'),
            onPressed: (){
              llave.remove('correo');
              var db = new DatabaseHelper();
              db.formatear();
              Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder:(context)=>page), (Route<dynamic> route) => false);
            },
          )
          ],

        );
      }
  );
}



Future<void> recordarContra(context)async {
  try{
    await FirebaseAuth.instance.sendPasswordResetEmail(email: null);
    Navigator.pop(context);
  }catch(e){

  }
}

