//Librerias
import 'package:flutter/material.dart';
import 'package:bhelp/screens/agenda/agenda.dart';
import 'package:bhelp/screens/autorizar.dart';
import 'package:bhelp/screens/busquedas.dart' ;
import 'package:bhelp/screens/rompecabezas/inicioPuzzle.dart' as puzzle;
import 'package:bhelp/screens/identificacionColor/tutorial.dart' as tutorial;
import 'package:bhelp/screens/especialistas.dart' as espe;
import 'package:bhelp/screens/hijos/presentacion_hijos.dart' as PH;
import 'package:bhelp/screens/autenticacion/login.dart';
import 'package:bhelp/main.dart';
// Widgets utilizados
import 'ListView.dart' as Lista;

Drawer menu(BuildContext context){
  return  Drawer(
      child: new ListView(
          children:<Widget>[
            new UserAccountsDrawerHeader(
              accountName: Text("Usuario"),
              accountEmail: Text(sharedPreferencesFull.getString('correo')),
            ),
            Lista.opcionMenuCon(Icon(Icons.event_available), 'Agenda',context, AgendaPage(),'En este momento no tiene conexión a internet, los cambios se efectuarán automáticamente cuando se recupere'),
            //Lista.opcionMenu(Icon(Icons.accessibility), 'Crecimento y desarrollo', context, Agenda() ),
            //Lista.opcionMenu(Icon(Icons.notifications_active),'Emergencias', context, Agenda()),
            Lista.opcionMenu(Icon(Icons.extension),'Juegos cognitivos', context, puzzle.MyApp()),
            //Lista.opcionMenu(Icon(Icons.videogame_asset),'Juego de memoria', context, memoria.HomePage()),
            Lista.opcionMenu(Icon(Icons.color_lens),'Sensibilidad a colores', context, tutorial.TutorialPage()),
            Lista.opcionMenuCon(Icon(Icons.contact_mail),'Especialistas', context, espe.Especialistas(),'En este momento no tiene conexión a internet, se mostraran los especialistas almacenados localmente'),
            Lista.opcionMenuCon(Icon(Icons.child_care),'Hijos', context, PH.HijosVistaPage(),'En este momento no tiene conexion a internet, los cambios se efectuarán automáticamente cuando se recupere'),
            Lista.opcionMenuCon(Icon(Icons.info), 'Información',context, BusquedaPage(),'En este momento no tiene conexión a internet'),
            Lista.opcionMenuAutorizar(Icon(Icons.share),'Autorizar cuenta', context),
            Lista.opcionClose(Icon(Icons.exit_to_app),'Cerrar sesión', context, Loging(sharedPreferencesFull),sharedPreferencesFull),

          ]
      )
  );
}


