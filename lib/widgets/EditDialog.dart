import 'package:flutter/material.dart';

Future<bool> editDialog(BuildContext context, String imagen, String titulo, String contenido) {
  return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return new AlertDialog(
          title: new Row(children: <Widget>[
            new Image.asset((imagen),
              width: 85,height: 85,
            ),
            Text('  '),
            new Expanded(child:Text(titulo),),
          ],
          ),
          content: new Text(contenido),

          actions: <Widget>[
            new FlatButton(
              child: new Text('Sí', style: TextStyle(color: Colors.green, fontSize: 17, fontWeight: FontWeight.bold),),
              onPressed: () {
                Navigator.of(context).pop(true);
              },
            ),
            new FlatButton(
              child: new Text('No', style: TextStyle(color: Colors.green, fontSize: 17, fontWeight: FontWeight.bold),),
              onPressed: () {
                Navigator.of(context).pop(false);
              },
            )
          ],
        );
      }
  );
}
