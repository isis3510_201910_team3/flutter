import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:bhelp/DAOS/Vacuna.dart';
import 'package:bhelp/presenters/visualizaPantalla.dart';

var idActual;

class VacunasList extends StatelessWidget {
  List<Vacuna> item;
  VisualizaPantalla visualizacionPantalla;
  void accion;

  VacunasList(List<Vacuna> this.item,
      VisualizaPantalla this.visualizacionPantalla,
      void this.accion,
      {
        Key key,
      }) : super(key: key);

  @override
  Widget build(BuildContext context,) {
    return new ListView.builder(
      shrinkWrap: true,
        itemCount: item == null ? 0 : item.length,
        itemBuilder: (BuildContext context, int index) {
          idActual = index;
          return new Card(
            child: new Container(
                child: new Center(
                  child: new Row(
                    children: <Widget>[
                      new Checkbox(
                        value: false,
                        onChanged:(value) => accion,
                      ),
                      new Expanded(
                        child: new Padding(
                          padding: EdgeInsets.all(10.0),
                          child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Text(
                                (item[index].nombre),
                                // set some style to text
                                style: new TextStyle(
                                    fontSize: 16.0, color: Colors.black),
                              ),
                              new Text(
                                "Edad: " + (item[index].edad).toString() + " meses",
                                // set some style to text
                                style: new TextStyle(
                                    fontSize: 15.0, color: Colors.black54),
                              ),
                            ],
                          ),
                        ),
                      ),

                    ],
                  ),
                ),
                padding: const EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0)
            ),
          );
        });
  }

}