import 'package:flutter/material.dart';
import 'package:bhelp/presenters/home.dart';
import 'package:shared_preferences/shared_preferences.dart';



final Color rosa= Color.fromARGB(255, 254, 90, 90);
final Color menta= Color.fromARGB(255, 15, 209, 144);
final Color gris= Colors.black54;
final double tamanioTexto =16;
SharedPreferences sharedPreferencesFull=null;


void main() async {
  final sharedPreferencesFirst = await SharedPreferences.getInstance();
  sharedPreferencesFull=sharedPreferencesFirst;

  runApp(MyApp(sharedPreferencesFirst));
}

class MyApp extends StatelessWidget {
  SharedPreferences sharedPreferences;
  MyApp(this.sharedPreferences);

  // This widget is the root of your application
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'BHELP Demo',
      theme: ThemeData(
          primaryColor: rosa,
          fontFamily: 'Roboto'
      ),

      home: inicio(sharedPreferences),
    );
  }



}



