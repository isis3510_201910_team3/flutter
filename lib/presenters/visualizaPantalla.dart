import 'package:bhelp/model/database_helper.dart';
import 'package:bhelp/DAOS/Tareas.dart';
import 'package:bhelp/DAOS/Hijo.dart';
import 'package:bhelp/DAOS/Vacuna.dart';
import 'package:bhelp/model/fireStore.dart';
import 'package:bhelp/main.dart';

abstract class Vista {
  void screenUpdate();
}

class VisualizaPantalla {
  Vista _view;
  var db = new DatabaseHelper();

  VisualizaPantalla(this._view);

  delete(Tareas tarea) async {
    var db = new DatabaseHelper();
    db.deleteTareas(tarea);
    await borrarTareaCategoria(tarea);
    await borrarTareaHijo(tarea);
    updateScreen();
  }

  deleteHijo(Hijo hijos) {
    var db = new DatabaseHelper();
    db.deleteHijos(hijos);
    borrarHijo(hijos);
    int numHijos=sharedPreferencesFull.getInt('hijos');
    sharedPreferencesFull.setInt('hijos', numHijos-1);
    updateScreen();
  }

  updateTarea(Tareas tarea)async {
    var db = new DatabaseHelper();
    db.update(tarea);

    updateScreen();
  }

  updateVacunaHijo(Hijo hijoActual,Hijo hijoNuevo)async {
    updateHijoFires(hijoActual, hijoNuevo);

    updateScreen();
  }


  updateHijo(Hijo hijo)async {
    var db = new DatabaseHelper();
    db.updateHijo(hijo);

    updateScreen();
  }

  Future<List<Tareas>> getTareas(tipo) {
    return db.getTareas(tipo);
  }

  Future<List<Tareas>> getTareasPrioridad(prioridad) {
    return db.getTareasPrioridad(prioridad);
  }

  Future<List<Hijo>> getHijos() {
    return db.getHijos();
  }

  updateScreen() {
    _view.screenUpdate();
  }
}
