import 'package:flutter/material.dart';
import 'package:bhelp/screens/agenda/agenda.dart';
import 'package:bhelp/screens/hijos/presentacion_hijos.dart';
import 'package:bhelp/screens/autenticacion/login.dart';
import 'package:shared_preferences/shared_preferences.dart';

// poner loging   HijosVistaPage para mirar el loging
Widget inicio( SharedPreferences sharedPreferences){
  int numHijos =sharedPreferences.getInt('hijos');
  if ( sharedPreferences.getString('correo') == null ){
    return Loging(sharedPreferences);
  }
  else if(numHijos != null && numHijos>0){
    return AgendaPage();
  }
  else {
    return HijosVistaPage();
  }
}
